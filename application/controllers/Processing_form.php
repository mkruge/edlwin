<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Processing_form extends CI_Controller {

  function __construct() {
    parent::__construct();
    date_default_timezone_set("Asia/Bangkok");

    //call database
    $this->load->model('general_model');
    $this->load->database();
  }

  function edit_my_account() {

    $package = array(
        'EmployeeName' => $this->input->post('EmployeeName'),
        'EmployeeNo' => $this->input->post('EmployeeNo'),
        'password' => $this->input->post('password'),
        'NIDN' => $this->input->post('nidn'),
        'Gender' => $this->input->post('gender'),
        'Birthday' => $this->input->post('Birthday'),
        'Birthplace' => $this->input->post('Birthplace'),
        'Email' => $this->input->post('email'),
        'GelarDepan' => $this->input->post('gelardepan'),
        'GelarBelakang' => $this->input->post('gelarbelakang'),
        'phone' => $this->input->post('phone'),
        'JoinDate' => $this->input->post('JoinDate'),
        'id_position' => $this->input->post('position'),
        'id_fakultas' => $this->input->post('fakultas'),
        'id_prodi' => $this->input->post('prodi'),
    );

    $this->db->where('EmployeeNo', $this->input->post('EmployeeNo'));
    $this->db->update('database', $package);

    redirect('general/display_account');
  }

  function admin_edit() {
    $get_id = $this->input->post('subject_lecture');


    $this->db->select('*');
    $this->db->from('database');
    $this->db->where('id_prodi = ' . $get_id . '');
    $this->db->where('EmployeeNo = ' . $get_id . '');

    $get_all = $this->db->get();
    $get_lecture = $get_all->result();

    $data = array(
        'subject' => $get_lecture,
        'prodi' => $this->general_model->getProdi(),
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('adminEdit', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  function admin_score() {
    $get_id = $this->input->post('subject_lecture');


    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('prodi');
    $this->db->from('semester');
    $this->db->where('id_prodi = ' . $get_id . '');

    $get_all = $this->db->get();
    $get_lecture = $get_all->result();

    $data = array(
        'subject' => $get_lecture,
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('adminEditScore', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function sendComment() {

    $this->load->library('email');
    $this->email->from('your@example.com', 'Your Name');
    $this->email->to('someone@example.com');
    $this->email->cc('another@another-example.com');
    $this->email->bcc('them@their-example.com');

    $this->email->subject('Email Test');
    $this->email->message('Testing the email class.');

    $this->email->send();
  }

  public function uploadFile() {
    $this->load->model('general_model');
    $this->load->model('Score');
    $this->load->model('KelasModel');

    require_once APPPATH . "/third_party/PHPExcel.php";
    if (isset($_POST['upload_score'])) {
      $config['upload_path'] = './uploads/';
      $config['allowed_types'] = 'gif|jpg|png|jpeg|xls|xlsx';
      //file apa saja yang diijinkan untuk diupload
      $config['max_size'] = 100;
      $config['max_width'] = 1024;
      $config['max_height'] = 768;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('userfile')) {
        $data = array('upload_data' => $this->upload->data());

        $data['sem'] = $this->input->post('semester_id') == '' ? 1 : $this->input->post('semester_id');
        $data['class'] = $this->input->post('class_id') == '' ? 1 : $this->input->post('class_id');
        $data['subject'] = $this->input->post('subject_id') == '' ? 1 : $this->input->post('subject_id');

        $PHPExcelReader = new PHPExcel_Reader_Excel2007();
        $excel = $PHPExcelReader->load($data['upload_data']['full_path']);
        $data['excel'] = $excel->getSheet(0)->toArray();

        $jdata = count($data['excel']) - 1;

        for ($i = 0; $i < $jdata; $i++) {
          for ($a = 5; $a <= 23; $a++) {
            if ($data['excel'][$i + 1][$a] > 0 && $data['excel'][$i + 1][$a] <= 22) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['sum'][$a][] = $data['excel'][$i + 1][$a];
              }
            }

            //7
            if ($a >= 5 && $a <= 11) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['information_provider'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //4
            if ($a >= 12 && $a <= 16) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['role_model'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //2
            if ($a >= 17 && $a <= 18) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['facilitator'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //4
            if ($a >= 19 && $a <= 22) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['assesor'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //1
            if ($a == 23) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['student_learning'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
          }
        }

        for ($x = 5; $x <= 23; $x++) {
          //all score
          if ($x <= 23) {
            $data['done']['sum'][$x] = array_sum($data['sum'][$x]) / count($data['sum'][$x]);
          }
          //information provider
          if ($x >= 5 && $x <= 11) {
            $data['done']['information_provider'][$x] = array_sum($data['information_provider'][$x]) / count($data['information_provider'][$x]);
          }
          //role model
          if ($x >= 12 && $x <= 16) {
            $data['done']['role_model'][$x] = array_sum($data['role_model'][$x]) / count($data['role_model'][$x]);
          }
          if ($x >= 17 && $x <= 18) {
            $data['done']['facilitator'][$x] = array_sum($data['facilitator'][$x]) / count($data['facilitator'][$x]);
          }
          if ($x >= 19 && $x <= 22) {
            $data['done']['assesor'][$x] = array_sum($data['assesor'][$x]) / count($data['assesor'][$x]);
          }
          if ($x == 23) {
            $data['done']['student_learning'][$x] = array_sum($data['student_learning'][$x]) / count($data['student_learning'][$x]);
          }
        }

        $data['sum'] = array_values($data['sum']);
        $data['done']['sum'] = array_values($data['done']['sum']);
        $data['done']['student_learning'] = array_values($data['done']['student_learning']);
        $data['done']['assesor'] = array_values($data['done']['assesor']);
        $data['done']['facilitator'] = array_values($data['done']['facilitator']);
        $data['done']['role_model'] = array_values($data['done']['role_model']);
        $data['done']['information_provider'] = array_values($data['done']['information_provider']);

        $lecturer_id = $_GET['id_lecture'];
        $code = time();

        for ($i = 0; $i <= 18; $i++) {
          $lecturer_id = $lecturer_id;
          $semester_id = $data['sem'];
          $class_id = $data['class'];
          $subject_id = $data['subject'];

          $score = isset($data['done']['sum'][$i]) ? $data['done']['sum'][$i] : 0;
          $information_provider = isset($data['done']['information_provider'][$i]) ? $data['done']['information_provider'][$i] : 0;
          $role_model = isset($data['done']['role_model'][$i]) ? $data['done']['role_model'][$i] : 0;
          $facilitator = isset($data['done']['facilitator'][$i]) ? $data['done']['facilitator'][$i] : 0;
          $assesor = isset($data['done']['assesor'][$i]) ? $data['done']['assesor'][$i] : 0;
          $student_learning = isset($data['done']['student_learning'][$i]) ? $data['done']['student_learning'][$i] : 0;

          $dx = array(
              'lecturer_id' => $lecturer_id,
              'semester_id' => $semester_id,
              'class_id' => $class_id,
              'subject_id' => $subject_id,
              'score' => $score,
              'information_provider' => $information_provider,
              'role_model' => $role_model,
              'facilitator' => $facilitator,
              'assesor' => $assesor,
              'score' => $score,
              'student_learning' => $student_learning,
              'total' => 0,
              'code' => $code,
              'file' => json_encode($data['sum']),
              'question_id' => ($i + 1)
          );
          $this->Score->add($dx);
        }
        redirect('scores/view/' . $code);
      }
    }

    $this->load->model('general_model');
    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('laporan');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->join('semester', 'laporan.kode_semester = semester.term_id');
    $this->db->where('EmployeeNo = ' . $_GET['id_lecture'] . '');

    $get_employee = $this->db->get();
    $employee_result['employee'] = $get_employee->result();
    $semester = isset($_GET['sem']) && $_GET['sem'] != '' ? $_GET['sem'] : null;
    $data = array(
        'employee_identity' => $employee_result['employee'],
        'subjects' => $this->general_model->getSubjects(),
        'kelas' => $this->KelasModel->find(),
        'sem' => $this->general_model->getSemesters($semester)
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('admin_upload_file', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function reuploadFile() {
    $this->load->model('general_model');
    $this->load->model('Score');

    require_once APPPATH . "/third_party/PHPExcel.php";
    if (isset($_POST['upload_score'])) {
      $code = $this->input->post('code');
      $this->db->query("delete from score WHERE code=" . $code);

      $config['upload_path'] = './uploads/';
      $config['allowed_types'] = 'gif|jpg|png|jpeg|xls|xlsx';
      //file apa saja yang diijinkan untuk diupload
      $config['max_size'] = 100;
      $config['max_width'] = 1024;
      $config['max_height'] = 768;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('userfile')) {
        $data = array('upload_data' => $this->upload->data());

        $data['sem'] = $this->input->post('semester_id') == '' ? 1 : $this->input->post('semester_id');
        $data['class'] = $this->input->post('class_id') == '' ? 1 : $this->input->post('class_id');
        $data['subject'] = $this->input->post('subject_id') == '' ? 1 : $this->input->post('subject_id');

        $PHPExcelReader = new PHPExcel_Reader_Excel2007();
        $excel = $PHPExcelReader->load($data['upload_data']['full_path']);
        $data['excel'] = $excel->getSheet(0)->toArray();

        $jdata = count($data['excel']) - 1;

        for ($i = 0; $i < $jdata; $i++) {
          for ($a = 5; $a <= 23; $a++) {
            if ($data['excel'][$i + 1][$a] > 0 && $data['excel'][$i + 1][$a] <= 22) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['sum'][$a][] = $data['excel'][$i + 1][$a];
              }
            }

            //7
            if ($a >= 5 && $a <= 11) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['information_provider'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //4
            if ($a >= 12 && $a <= 16) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['role_model'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //2
            if ($a >= 17 && $a <= 18) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['facilitator'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //4
            if ($a >= 19 && $a <= 22) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['assesor'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
            //1
            if ($a == 23) {
              if ($data['excel'][$i + 1][$a] > 0) {
                $data['student_learning'][$a][] = $data['excel'][$i + 1][$a];
              }
            }
          }
        }

        for ($x = 5; $x <= 23; $x++) {
          //all score
          if ($x <= 23) {
            $data['done']['sum'][$x] = array_sum($data['sum'][$x]) / count($data['sum'][$x]);
          }
          //information provider
          if ($x >= 5 && $x <= 11) {
            $data['done']['information_provider'][$x] = array_sum($data['information_provider'][$x]) / count($data['information_provider'][$x]);
          }
          //role model
          if ($x >= 12 && $x <= 16) {
            $data['done']['role_model'][$x] = array_sum($data['role_model'][$x]) / count($data['role_model'][$x]);
          }
          if ($x >= 17 && $x <= 18) {
            $data['done']['facilitator'][$x] = array_sum($data['facilitator'][$x]) / count($data['facilitator'][$x]);
          }
          if ($x >= 19 && $x <= 22) {
            $data['done']['assesor'][$x] = array_sum($data['assesor'][$x]) / count($data['assesor'][$x]);
          }
          if ($x == 23) {
            $data['done']['student_learning'][$x] = array_sum($data['student_learning'][$x]) / count($data['student_learning'][$x]);
          }
        }

        $data['sum'] = array_values($data['sum']);
        $data['done']['sum'] = array_values($data['done']['sum']);
        $data['done']['student_learning'] = array_values($data['done']['student_learning']);
        $data['done']['assesor'] = array_values($data['done']['assesor']);
        $data['done']['facilitator'] = array_values($data['done']['facilitator']);
        $data['done']['role_model'] = array_values($data['done']['role_model']);
        $data['done']['information_provider'] = array_values($data['done']['information_provider']);

        $lecturer_id = $_GET['id_lecture'];
        $code = time();

        for ($i = 0; $i <= 18; $i++) {
          $lecturer_id = $lecturer_id;
          $semester_id = $data['sem'];
          $class_id = $data['class'];
          $subject_id = $data['subject'];

          $score = isset($data['done']['sum'][$i]) ? $data['done']['sum'][$i] : 0;
          $information_provider = isset($data['done']['information_provider'][$i]) ? $data['done']['information_provider'][$i] : 0;
          $role_model = isset($data['done']['role_model'][$i]) ? $data['done']['role_model'][$i] : 0;
          $facilitator = isset($data['done']['facilitator'][$i]) ? $data['done']['facilitator'][$i] : 0;
          $assesor = isset($data['done']['assesor'][$i]) ? $data['done']['assesor'][$i] : 0;
          $student_learning = isset($data['done']['student_learning'][$i]) ? $data['done']['student_learning'][$i] : 0;

          $dx = array(
              'lecturer_id' => $lecturer_id,
              'semester_id' => $semester_id,
              'class_id' => $class_id,
              'subject_id' => $subject_id,
              'score' => $score,
              'information_provider' => $information_provider,
              'role_model' => $role_model,
              'facilitator' => $facilitator,
              'assesor' => $assesor,
              'score' => $score,
              'student_learning' => $student_learning,
              'total' => 0,
              'code' => $code,
              'file' => json_encode($data['sum']),
              'question_id' => ($i + 1)
          );
          $this->Score->add($dx);
        }
        redirect('scores/view/' . $code);
      }
    }

    $this->load->model('general_model');
    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('laporan');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->join('semester', 'laporan.kode_semester = semester.term_id');
    $this->db->where('EmployeeNo = ' . $_GET['id_lecture'] . '');

    $get_employee = $this->db->get();
    $employee_result['employee'] = $get_employee->result();
    $semester = isset($_GET['sem']) && $_GET['sem'] != '' ? $_GET['sem'] : null;
    $code = isset($_GET['code']) && $_GET['code'] != '' ? $_GET['code'] : null;
    $data = array(
        'employee_identity' => $employee_result['employee'],
        'subjects' => $this->general_model->getSubjects(),
        'sem' => $this->general_model->getSemesters($semester),
        'header_scores' => $this->Score->getHeader($code)
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('admin_reupload_file', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

// NEWS     NEWS     NEWS     NEWS     NEWS     NEWS     

  public function display_news() {

    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('berita');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_employee = $this->db->get();
    $employee_result['employee'] = $get_employee->result();


    $data = array(
        'news_identity' => $employee_result['employee']
    );


    $this->load->view('fix_template/global_header', $data);
    $this->load->view('adminEditNews', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function edit_news() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('berita');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_news = $this->db->get();
    $news_result = $get_news->result();



    $data = array(
        'news_identity' => $news_result
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('admin_edit_news', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  function edit_my_news() {

    $package = array(
        'news_id' => $this->input->post('news_id'),
        'news_title' => $this->input->post('news_title'),
        'isi_news' => $this->input->post('isi_news'),
    );


    $this->db->where('news_id', $this->input->post('news_id'));
    $this->db->update('berita', $package);

    redirect('processing_form/display_news');
  }

// Contact Us           Contact Us           Contact Us           Contact Us              

  function edit_contact() {

    $package = array(
        'contact_id' => $this->input->post('contact_id'),
        'contact_name' => $this->input->post('contact_name'),
        'contact_position' => $this->input->post('contact_position'),
    );


    $this->db->where('contact_id', $this->input->post('contact_id'));
    $this->db->update('contact_us', $package);

    redirect('processing_form/display_contact');
  }

  function display_contact() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('contact_us');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_all = $this->db->get();
    $get_contact = $get_all->result();

    $data = array(
        'contact' => $get_contact,
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('adminEditContact', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  function admin_contact_us() {

    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('contact_us');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');


    $get_all = $this->db->get();
    $get_contact = $get_all->result();



    $data = array(
        'contact' => $get_contact,
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('admin_edit_contact', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  function add_contact() {
    $package = array(
        'contact_name' => $this->input->post('contact_name'),
        'contact_position' => $this->input->post('contact_position'),
    );


    $this->db->where('contact_id', $this->input->post('contact_id'));
    $this->db->update('contact_us', $package);

    redirect('processing_form/display_contact');
  }

  public function adding_contact() {

    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('contact_us');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');


    $get_all = $this->db->get();
    $get_contact = $get_all->result();


    $data = array(
        'contact' => $get_contact,
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('admin_add_contact', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  function debug($v) {
    echo '<pre>';
    print_r($v);
    echo '</pre>';
  }

}

?>