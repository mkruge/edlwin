<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Contact extends CI_Controller {

      function __construct() {
          parent::__construct();
          date_default_timezone_set("Asia/Bangkok");

          //call database
          $this->load->model('ContactModel');
          $this->load->database();

          if (!$this->session->userdata('employeeno')) {
              redirect(base_url("login_process"));
          }
      }

      function index() {
          $data = [
            'contact' => $this->ContactModel->find()
          ];
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('contact/index', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function add() {
          $data = [
          ];
          $this->load->helper(array('form', 'url'));
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('contact/add', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function create() {

          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
          $this->form_validation->set_rules('name', 'Title', 'required');
          $this->form_validation->set_rules('position', 'Isi', 'required');

          if ($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('msg', 'Isian belum lengkap. Pls fix the error(s)');
              redirect('contact/add');
          }

          $data = array(
            'contact_name' => $this->input->post('name'),
            'contact_position' => $this->input->post('position')
          );
          $this->db->insert('contact_us', $data);
          redirect('contact/index');
      }

      function edit($id) {
          $data = [
            'contact' => $this->ContactModel->findFirstByid($id)
          ];
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('contact/edit', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function update() {
          $data = array(
            'contact_name' => $this->input->post('name'),
            'contact_position' => $this->input->post('position')
          );
          $this->db->where('contact_id', $this->input->post('id'));
          $this->db->update('contact_us', $data);
          redirect('contact/index');
      }

      public function delete($id) {
          $this->db->where('contact_id', $id);
          if($this->db->delete('contact_us')){
            $this->session->set_flashdata('msg', 'ID #'.$id." successfully deleted!");
            redirect('contact/index');
          }
      }

// Login 		Login 		Login 		Login 		Login 		
      function masuk() {
          //http://localhost/uph/general/masuk
          redirect(base_url() . 'index');
      }

      function form_validation() {
          $this->load->library('form_validation');
          $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          $this->form_validation->set_rules("Birthday", "Birthday", 'required|alpha');
          #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          if ($this->form_validation->run()) {
              //true  
              $this->load->model("general_model");
              $data = array(
                "EmployeeNo" => $this->input->post("employeeno")
              );
          } else {
              //false  
              $this->masuk();
          }
      }

      public function my_account() {

          $this->load->helper('form');
          $this->load->library('form_validation');
          $this->load->model('general_model');
          $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');
          $get_employee = $this->db->get();
          $res = $get_employee->result();
          $positions = $this->general_model->getPositions();
          $data = array(
            'employee_identity' => $res,
            'positions' => $this->general_model->getPositions(),
            'fakultas' => $this->general_model->getFakultas(),
            'prodi' => $this->general_model->getProdi()
          );
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('myaccount', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function display_account() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();
          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('display', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function contact_us() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->from('contact_us');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_all = $this->db->get();
          $get_contact = $get_all->result();

          $data = array(
            'contact' => $get_contact,
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('contactus', $data);
          $this->load->view('fix_template/global_contact_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

//	Score 		Score 		Score 		Score 		Score 		
      public function all_score_page_kaprodi() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('allscore_kaprodi', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function all_score_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );
          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('allscore', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_linechart_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scorelinechart', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_graphic_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scoregraphic', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_table_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scoretable', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

  }
  