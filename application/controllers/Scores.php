<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Scores extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");

        //call database
        $this->load->model('general_model');
        $this->load->model('Score');
        $this->load->model('Comment');
        $this->load->database();

        if (!$this->session->userdata('employeeno')) {
            redirect(base_url("login_process"));
        }
    }

    function export_pdf() {
        require_once APPPATH . "/third_party/xtcpdf.php";
        $employee_id = $this->session->userdata('employeeno');
        $user = $this->general_model->getEmployeeData($employee_id);

        $g_employee = $user->user_type == '1' ? $employee_id : null;
        $g_prodi = isset($_GET['prodi']) && $_GET['prodi'] != '' ? $_GET['prodi'] : null;
        $g_fac = isset($_GET['faculty']) && $_GET['faculty'] != '' ? $_GET['faculty'] : null;

        $scores = $this->Score->find($g_employee, $g_fac, $g_prodi);
        #$this->debug($scores);
        #die;
        
        set_time_limit(300000);
        $posisi = isset($_GET['posisi']) && $_GET['posisi'] != '' ? $_GET['posisi'] : 'P';
        $pdf = new CantikPdf($posisi, PDF_UNIT, 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mizno Kruge');
        $pdf->SetTitle('TCPDF Example 048');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
//$pdf->SetHeaderData($logo,20, '','');
// set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
#$pdf->SetDisplayMode($zoom='fullpage', $layout='TwoColumnRight', $mode='UseNone');
// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->setPrintFooter(FALSE);
//set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 28, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();
        $pdf->SetFont('helvetica', '', 8);
//---------------------------------------------------------- config end ---------------------------------------------------------------------------

        $html = '<center><h1>Laporan Buku</h1></center>';
        $html .= '<table border="1" cellpadding="5" cellspacing="0" width="100%">';
        $html .= '<tr>';
        $html .= '<td width="50">Employee No</td>';
        $html .= '<td width="200">Employee Name</td>';
        $html .= '<td width="100">Kode Register</td>';
        $html .= '<td width="100">Judul</td>';
        $html .= '<td width="100">Score</td>';
        $html .= '</tr>';
        $no = 1;
        foreach($scores as $score) {
            $html .= '<tr>';
            $html .= '<td>' . $score->EmployeeNo . '</td>';
            $html .= '<td>' . $score->EmployeeName . '</td>';
            $html .= '<td>' . $score->Email . '</td>';
            $html .= '<td>' . $score->academicName_fakultas . '</td>';
            $html .= '<td>' . $score->avg_score . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $pdf->writeHTML($html);
        $pdf->Output('Laporan Koleksi.pdf', 'I');
//I: output ke browser
//D: Download
//F: output ke file di server
    }

    function upload_comment($id = null) {
        $this->load->model('general_model');
        if (isset($_POST['submit'])) {
            $config['upload_path'] = './uploads/student_comments/';
            $config['allowed_types'] = 'pdf|xlsx|xls';
            //file apa saja yang diijinkan untuk diupload
            if (!file_exists($config['upload_path'])) {
                @mkdir($config['upload_path'], 0777, true);
            }

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('userfile')) {
                $upload_data = $this->upload->data();
                $data = ['score_code' => $id, 'full_path' => $upload_data['full_path'], 'filename' => $upload_data['file_name']];
                $this->Score->saveStudentComment($data);
                redirect('scores/view/' . $id);
            } else {
                var_dump($this->upload->display_errors());
                die;
            }
        }
        $result = $this->general_model->fetch_single_data($_SESSION['employeeno']);
        $data = [
            'header_scores' => $this->Score->getHeader($id),
            'scores' => $this->Score->findBycode($id),
            'questions' => $this->Score->getQuestions(),
            'comments' => $this->Comment->findByScoreId($id),
            'logindata' => $result->result()
        ];
        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scores/upload_comment', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    function index() {

        $employee_id = $this->session->userdata('employeeno');
        $user = $this->general_model->getEmployeeData($employee_id);

        $g_employee = $user->user_type == '1' ? $employee_id : null;
        $g_prodi = isset($_GET['prodi']) && $_GET['prodi'] != '' ? $_GET['prodi'] : null;
        $g_fac = isset($_GET['faculty']) && $_GET['faculty'] != '' ? $_GET['faculty'] : null;

        $scores = $this->Score->find($g_employee, $g_fac, $g_prodi);
        $data = [
            'scores' => $scores,
            'user' => $user,
            'prodies' => $this->general_model->getProdi(),
            'faculties' => $this->general_model->getFakultas()
        ];
        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scores/index', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    function add() {
        $data = [
        ];
        $this->load->helper(array('form', 'url'));
        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scores/add', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    function send_comment() {
        $data = array(
            'score_id' => $this->input->post('score_id'),
            'comment' => $this->input->post('comment'),
            'user_id' => isset($_SESSION['employeeno']) ? $_SESSION['employeeno'] : 1,
            'created' => date('Y-m-d H:i:s')
        );
        $this->db->insert('comments', $data);
        redirect('scores/view/' . $this->input->post('score_id'));
    }

    function create() {

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('scores_title', 'Title', 'required');
        $this->form_validation->set_rules('isi_scores', 'Isi', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', 'Isian belum lengkap. Pls fix the error(s)');
            redirect('scores/add');
        }

        $data = array(
            'scores_title' => $this->input->post('scores_title'),
            'isi_scores' => $this->input->post('isi_scores'),
            'staff' => isset($_SESSION['employeeno']) ? $_SESSION['employeeno'] : 1
        );
        $this->db->insert('berita', $data);
        redirect('scores/index');
    }

    function edit($id) {
        $data = [
            'scores' => $this->Score->findFirstByid($id)
        ];
        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scores/edit', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    function view($id) {

        $this->load->model('general_model');
        $result = $this->general_model->fetch_single_data($_SESSION['employeeno']);
        $data = [
            'header_scores' => $this->Score->getHeader($id),
            'scores' => $this->Score->findBycode($id),
            'questions' => $this->Score->getQuestions(),
            'comments' => $this->Comment->findByScoreId($id),
            'logindata' => $result->result(),
            'studentComment' => $this->Score->getStudentComment($id)
        ];
        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scores/view', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    function update() {
        $data = array(
            'scores_title' => $this->input->post('title'),
            'isi_scores' => $this->input->post('isi')
        );
        $this->db->where('scores_id', $this->input->post('id'));
        $this->db->update('berita', $data);
        redirect('scores/index');
    }

    public function delete($id) {
        $this->db->where('scores_id', $id);
        $this->db->delete('berita');
        redirect('scores/index');
    }

// Login 		Login 		Login 		Login 		Login 		
    function masuk() {
        //http://localhost/uph/general/masuk
        redirect(base_url() . 'index');
    }

    function form_validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
        $this->form_validation->set_rules("Birthday", "Birthday", 'required|alpha');
        #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
        #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
        if ($this->form_validation->run()) {
            //true  
            $this->load->model("general_model");
            $data = array(
                "EmployeeNo" => $this->input->post("employeeno")
            );
        } else {
            //false  
            $this->masuk();
        }
    }

    public function my_account() {

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('general_model');
        $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
        $this->db->select('*');
        $this->db->from('database');
        $this->db->join('position', 'database.id_position = position.post_id');
        $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
        $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');
        $get_employee = $this->db->get();
        $res = $get_employee->result();
        $positions = $this->general_model->getPositions();
        $data = array(
            'employee_identity' => $res,
            'positions' => $this->general_model->getPositions(),
            'fakultas' => $this->general_model->getFakultas(),
            'prodi' => $this->general_model->getProdi()
        );
        $this->load->view('fix_template/global_header', $data);
        $this->load->view('myaccount', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    public function display_account() {
        $this->load->model('general_model');

        $this->db->select('*');
        $this->db->from('database');
        $this->db->join('position', 'database.id_position = position.post_id');
        $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
        $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

        $get_employee = $this->db->get();
        $employee_result['employee'] = $get_employee->result();
        $data = array(
            'employee_identity' => $employee_result['employee']
        );

        $this->load->view('fix_template/global_header', $data);
        $this->load->view('display', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    public function contact_us() {
        $this->load->model('general_model');

        $this->db->select('*');
        $this->db->from('database');
        $this->db->from('contact_us');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

        $get_all = $this->db->get();
        $get_contact = $get_all->result();

        $data = array(
            'contact' => $get_contact,
        );

        $this->load->view('fix_template/global_header', $data);
        $this->load->view('contactus', $data);
        $this->load->view('fix_template/global_contact_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

//	Score 		Score 		Score 		Score 		Score 		
    public function all_score_page_kaprodi() {
        $this->load->model('general_model');

        $this->db->select('*');
        $this->db->from('database');
        $this->db->join('position', 'database.id_position = position.post_id');
        $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
        $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

        $get_employee = $this->db->get();
        $employee_result['employee'] = $get_employee->result();



        $data = array(
            'employee_identity' => $employee_result['employee']
        );

        $this->load->view('fix_template/global_header', $data);
        $this->load->view('allscore_kaprodi', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    public function all_score_page() {
        $this->load->model('general_model');

        $this->db->select('*');
        $this->db->from('database');
        $this->db->join('position', 'database.id_position = position.post_id');
        $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
        $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

        $get_employee = $this->db->get();
        $employee_result['employee'] = $get_employee->result();

        $data = array(
            'employee_identity' => $employee_result['employee']
        );
        /* testing database
          echo '<pre>';
          print_r($getcontact['mycontact']);
          die();
         */

        $this->load->view('fix_template/global_header', $data);
        $this->load->view('allscore', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    public function score_linechart_page() {
        $this->load->model('general_model');

        $this->db->select('*');
        $this->db->from('database');
        $this->db->join('position', 'database.id_position = position.post_id');
        $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
        $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

        $get_employee = $this->db->get();
        $employee_result['employee'] = $get_employee->result();



        $data = array(
            'employee_identity' => $employee_result['employee']
        );

        /* testing database
          echo '<pre>';
          print_r($getcontact['mycontact']);
          die();
         */

        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scorelinechart', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    public function score_graphic_page() {
        $this->load->model('general_model');

        $this->db->select('*');
        $this->db->from('database');
        $this->db->join('position', 'database.id_position = position.post_id');
        $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
        $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

        $get_employee = $this->db->get();
        $employee_result['employee'] = $get_employee->result();



        $data = array(
            'employee_identity' => $employee_result['employee']
        );

        /* testing database
          echo '<pre>';
          print_r($getcontact['mycontact']);
          die();
         */

        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scoregraphic', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }

    public function score_table_page() {
        $this->load->model('general_model');

        $this->db->select('*');
        $this->db->from('database');
        $this->db->join('position', 'database.id_position = position.post_id');
        $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
        $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
        $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

        $get_employee = $this->db->get();
        $employee_result['employee'] = $get_employee->result();



        $data = array(
            'employee_identity' => $employee_result['employee']
        );

        /* testing database
          echo '<pre>';
          print_r($getcontact['mycontact']);
          die();
         */

        $this->load->view('fix_template/global_header', $data);
        $this->load->view('scoretable', $data);
        $this->load->view('fix_template/global_footer', $data);
        $this->load->view('fix_template/global_foot', $data);
    }
    
    function debug($v) {
        echo '<pre>';
        print_r($v);
        echo '</pre>';
    }


}
