<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Login_process extends CI_Controller {

      function __construct() {
          parent::__construct();
          date_default_timezone_set("Asia/Bangkok");

          //call database
          $this->load->model('general_model');
          $this->load->database();
      }

      public function index() {
//    public function log_in()
          $this->load->view('fix_template/global_login_header');
          $this->load->view('login');
          $this->load->view('fix_template/global_footer');
          $this->load->view('fix_template/global_foot');
      }

      function login_validation() {
          $this->load->library('form_validation');
          $this->form_validation->set_rules('employeeno', 'Employeeno', 'required');
          $this->form_validation->set_rules('password', 'Password', 'required');

          if ($this->form_validation->run()) {
              //true
              $EmployeeNo = $this->input->post('employeeno');
              $password = $this->input->post('password');

              //model function
              $this->load->model('general_model');

              if ($this->general_model->can_login($EmployeeNo, $password) == true) {
                  $this->db->select('*');
                  $this->db->from('database');
                  $this->db->where('EmployeeNo = ' . $EmployeeNo . '');

                  $get_data = $this->db->get();
                  $get_employee_data = $get_data->result();

                  foreach ($get_employee_data as $employee_detail)
                      $session_data = array(
                        'employeeno' => $EmployeeNo,
                        'EmployeeName' => $employee_detail->EmployeeName,
                      );
                  $this->session->set_userdata($session_data);
                  redirect(base_url() . 'general/enter');
              } else {    
                  $this->session->set_flashdata('error', 'Invalid Username and Password');
                  redirect(base_url() . 'login_process/index');
              }
          } else {
              //false
              $this->masuk();
          }
      }

      function editscore_button() {

          if (!isset($_GET['subject_lecture']) || $_GET['subject_lecture'] == '') {
              header("Location:" . $_SERVER['HTTP_REFERER']);
          }

          $id = $_GET['subject_lecture'];

          $this->load->model('general_model'); //load model
          $data['hasilTransaksi'] = $this->general_model->getTransaksi($id);
            //membuat data dari hasil transaksi masuk ke $data

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('adminEditScore', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);




          /* $this->db->select('*');
            $this->db->from('database');
            $this->db->from('prodi');
            $this->db->where('EmployeeNo = '.$_SESSION['employeeno'].'');
            $this->db->where('prodi_id');

            $get_data = $this->db->get();
            $get_score_data = $get_data->result();

            foreach($get_score_data as $score_detail)


            $session_data = array(
            'prodi_id'    => $prodi_id,
            'academicName_prodi'  => $score_detail->academicName_prodi,
            );

            echo '<pre>';
            echo $this->db->last_query();
            print_r($get_score_data);
            die();

            $this->session->set_userdata($session_data);
            redirect(base_url() . 'processing_form/admin_score'); */
      }

  }

?>