<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Home extends CI_Controller {

      function __construct() {
          parent::__construct();
          date_default_timezone_set("Asia/Bangkok");

          //call database
          $this->load->model('general_model');
      }

// Login 		Login 		Login 		Login 		Login 		
      function masuk() {
          //http://localhost/ci_test/general/masuk
          $data['title'] = 'Simple Form';
          $this->load->view("masuk", $data);
      }

//	Pages		Pages		Pages		Pages		Pages

      public function log_in() {
          $this->load->view('fix_template/global_login_header');
          $this->load->view('login');
          $this->load->view('fix_template/global_footer');
          $this->load->view('fix_template/global_foot');
      }

      public function index() {

          $this->load->model('NewsModel');
          $this->load->database();
          $data = [
            'headline' => $this->NewsModel->findFirstbyHeadline(),
            'news'=>$this->NewsModel->find()
          ];
          
          $this->load->view('fix_template/global_header');
          $this->load->view('home', $data);
          $this->load->view('fix_template/global_footer');
          $this->load->view('fix_template/global_foot');
      }

      public function my_account() {
          $this->load->view('fix_template/global_header');
          $this->load->view('myaccount');
          $this->load->view('fix_template/global_footer');
          $this->load->view('fix_template/global_foot');
      }

      public function contact_us() {
          $this->load->view('fix_template/global_header');
          $this->load->view('contactus');
          $this->load->view('fix_template/global_contact_footer');
          $this->load->view('fix_template/global_foot');
      }

      public function score_page() {
          $getcontact['mycontact'] = $this->general_model->select_data();

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $data = array(
            'contact_list' => $getcontact['mycontact']
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('score', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

  }
  