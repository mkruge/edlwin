<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {

  public $user_id; //prop

  function __construct() {
    parent::__construct();
    date_default_timezone_set("Asia/Bangkok");
    $this->user_id = $_SESSION['employeeno'];
    //call database
    $this->load->model('general_model');
    $this->load->database();
    $this->load->model('Score');

    if (!$this->session->userdata('employeeno')) {
      redirect(base_url("login_process"));
    }
  }

  function update() {
    $user = $this->uri->segment(3);
    $this->load->model(general_model);
    $data["user_data"] = $this->main_model->fetch_single_data($user_id);
    $data["fetch_data"] = $this->main_model->fetch_data();
    $this->load->view("myaccount", $data);
  }

// Login 		Login 		Login 		Login 		Login 		
  function masuk() {
    //http://localhost/uph/general/masuk
    redirect(base_url() . 'index');
  }

  function form_validation() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
    $this->form_validation->set_rules("Birthday", "Birthday", 'required|alpha');
    #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
    #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
    if ($this->form_validation->run()) {
      //true  
      $this->load->model("general_model");
      $data = array(
          "EmployeeNo" => $this->input->post("employeeno")
      );
    } else {
      //false  
      $this->masuk();
    }
  }

  /*
    // ini di komen karena buat validation perlu di controller lain, karena kalo disini bakal di tanggal sama session



    function login_validation()
    {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('employeeno', 'Employeeno', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');

    if($this->form_validation->run())
    {
    //true
    $EmployeeNo = $this->input->post('employeeno');
    $password 	= $this->input->post('password');

    //model function
    $this->load->model('general_model');

    if($this->general_model->can_login($EmployeeNo, $password) == true)
    {
    $this->db->select('*');
    $this->db->from('database');
    $this->db->where('EmployeeNo = '.$EmployeeNo.'');

    $get_data = $this->db->get();
    $get_employee_data = $get_data->result();

    foreach($get_employee_data as $employee_detail)


    $session_data = array(
    'employeeno' => $EmployeeNo,
    'EmployeeName' => $employee_detail->EmployeeName,
    );


    $this->session->set_userdata($session_data);
    redirect(base_url() . 'general/enter');
    }
    else
    {
    redirect(base_url() . 'general/masuk');
    $this->session->set_flashdata('error', 'Invalid Username and Password');
    }
    }
    else
    {
    //false
    $this->masuk();
    }
    } */

  function enter() {




    if ($this->session->userdata('employeeno') != '') {
      redirect(base_url() . 'home');
      echo '<label><a href="' . base_url() . 'general/logout">Logout</a></label>';
    } else {
      redirect(base_url() . 'login');
    }
  }

  function logout() {
    $this->session->sess_destroy();
    redirect('login_process');
  }

  function can_login($EmployeeNo, $password) {
    $this->db->where('employeeno', $EmployeeNo);
    $this->db->where('password', $password);
    $query = $this->db->get('database');

    //SELECT * FROM employeeno WHERE employeeno = '$EmployeeNo' AND password = '$password'

    if ($query->num_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

//	Admin 		Admin 		Admin 		Admin 		Admin 		
  public function my_admin() {
    $this->load->model('general_model');
    $data['data'] = $this->general_model->prodi_data();

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('admin', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function my_admin_edit() {
    $this->load->model('general_model');
    $data['data'] = $this->general_model->prodi_data();

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('adminEdit', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  /*
    public function my_admin_score()
    {
    $this->load->model('general_model');
    $data['data'] = $this->general_model->prodi_data();

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('adminEditScore', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
    }
   */

//	Pages		Pages		Pages		Pages		Pages


  public function home_page() {



    $this->load->model('NewsModel');
    $data = array(
        'headline' => $this->NewsModel->findFirstbyHeadline(),
        'news' => $this->NewsModel->find()
    );


//    public function index()
    $this->load->view('fix_template/global_header');
    $this->load->view('home', $data);
    $this->load->view('fix_template/global_footer');
    $this->load->view('fix_template/global_foot');
  }

  public function my_account() {

    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('general_model');
    $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $this->user_id . '');
    $get_employee = $this->db->get();
    $res = $get_employee->result()[0];

    if ($res->user_type == 5) {
      $positions = $this->general_model->getPositions(true);
    } else {
      $positions = $this->general_model->getPositions();
    }
    $data = array(
        'employee_identity' => $res,
        'positions' => $positions,
        'fakultas' => $this->general_model->getFakultas(),
        'prodi' => $this->general_model->getProdi()
    );
    $this->load->view('fix_template/global_header', $data);
    $this->load->view('myaccount', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function display_account() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_employee = $this->db->get();
    $employee_result['employee'] = $get_employee->result();
    $data = array(
        'employee_identity' => $employee_result['employee']
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('display', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function contact_us() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('contact_us');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_all = $this->db->get();
    $get_contact = $get_all->result();

    $data = array(
        'contact' => $get_contact,
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('contactus', $data);
    $this->load->view('fix_template/global_contact_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

//	Score 		Score 		Score 		Score 		Score 		
  public function all_score_page_kaprodi() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_employee = $this->db->get();
    $res = $get_employee->result();
    $data = array(
        'identity' => $res
    );

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('allscore_kaprodi', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function all_score_page() {

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('database.EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_employee = $this->db->get();
    $res = $get_employee->result()[0];

    $this->load->model('general_model');
    $this->load->model('Score');

    if ($res->user_type == '1') {
      $prodi = $this->general_model->getProdiByFac($res->fakultas_id);
      $scores = $this->Score->groupByEmployee($res->fakultas_id);
      $chart = $this->Score->groupByEmployee($res->fakultas_id);
    } elseif ($res->user_type == '2') {
      $scores = $this->Score->groupByEmployee($res->fakultas_id);
      $chart = $this->Score->groupByEmployee($res->fakultas_id);
      $prodi = false;
    } elseif ($res->user_type == '5') {
      $prodi = $this->general_model->getProdiByFac();
      $scores = $this->Score->groupByEmployee();
      $chart = $this->Score->groupByEmployee();
    }

    if ($res->user_type == '3') {
      //if dosen, group by semester
      $chart = $this->Score->groupByTerm();
    }

    $semesters = $this->general_model->getSemesters();
    $cdata = [];
    $sdata = [];
    $x = 0;

    $lapData = [];
    if ($res->user_type == '1') {

      $filter = [];

      if (isset($_GET['facultyId'])) {
        $filter['facultyId'] = $_GET['facultyId'];
      }
      if (isset($_GET['prodi']) && $_GET['prodi'] != '') {
        $filter['prodiId'] = $_GET['prodi'];
      }
      if (isset($_GET['nama_dosen']) && $_GET['nama_dosen'] != '') {
        $filter['empName'] = $_GET['nama_dosen'];
      }
      $teachers = $this->general_model->getAllEmployeeData($filter);
    } elseif ($res->user_type == '2') {
      $teachers = $this->general_model->getAllEmployee(null, $res->id_prodi);
    } elseif ($res->user_type == '5') {
      $teachers = $this->general_model->getAllEmployee();
    }

    foreach ($teachers as $t) {
      $cdata['series'][$t->EmployeeNo]['data'][$x] = rand(1, 5);
      $cdata['series'][$t->EmployeeNo]['name'] = "'" . $t->EmployeeName . "'";

      $this->db->select('*,'
              . 'AVG(NULLIF(score.score,0)) as avg_score,'
              . 'AVG(NULLIF(score.information_provider,0)) as avg_information_provider,'
              . 'AVG(NULLIF(score.role_model,0)) as avg_role_model,'
              . 'AVG(NULLIF(score.facilitator,0)) as avg_facilitator,'
              . 'AVG(NULLIF(score.assesor,0)) as avg_assesor,'
              . 'AVG(NULLIF(score.student_learning,0)) as avg_student_learning');
      $this->db->from('score');
      $this->db->join('subjects', 'subjects.subject_id=score.subject_id');
      $this->db->join('semester', 'semester.term_ID=score.semester_id');
      $this->db->where('score.question_id <=', 18);
      $this->db->where('score.lecturer_id', $t->EmployeeNo);
      $this->db->group_by('score.code');
      $query = $this->db->get();
      $r = $query->result();

      if (count($r) > 0) {
        $total_score = 0;
        $i = 0;
        foreach ($r as $ss) {
          $total_score += $ss->avg_score;
          $r[$i]->avg_score = $this->getActualScore($ss->code);
          $i++;
        }
        $t->totalScore = $total_score / count($r);
      } else {
        $t->totalScore = 0;
      }
      $t->data = $r;
      $lapData[$t->EmployeeNo] = $t;
    }

    foreach ($semesters as $sem) {
      $sdata[] = "'" . $sem->term_title . " " . $sem->year . "'";

      $this->db->select('*,AVG(NULLIF(score.score,0)) as avg_score');
      $this->db->from('score');
      $this->db->join('database', 'database.EmployeeNo=score.lecturer_id', 'left');
      $this->db->where('semester_id', $sem->term_ID);
      $this->db->group_by('score.lecturer_id');
      $query = $this->db->get();
      $r = $query->result();
      $i = 0;
      foreach ($r as $j) {
        $cdata['series'][$j->lecturer_id]['name'] = "'" . $j->EmployeeName . "'";
        $cdata['series'][$j->lecturer_id]['data'][] = doubleval(number_format($j->avg_score, 2));
        $i++;
      }
      $x++;
    }
    $cdata['categories'] = implode(',', $sdata);
    $cdata['series'] = array_values($cdata['series']);

    $data = array(
        'identity' => $res,
        'prodi' => $prodi,
        'scores' => $scores,
        'chart' => $chart,
        'cdata' => $cdata,
        'data' => $lapData
    );
    $this->load->view('fix_template/global_header', $data);
    $this->load->view('allscore', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function all_score_by_lecturer($employeeNo) {

    $empID = $employeeNo;

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('database.EmployeeNo = ' . $employeeNo . '');

    $get_employee = $this->db->get();
    $res = $get_employee->result()[0];

    $sem = null;
    if (isset($_GET['sem']) && $_GET['sem'] != ''):
      $sem = $_GET['sem'];
    endif;

    $this->load->model('general_model');
    $this->load->model('Score');
    $prodi = $this->general_model->getProdiByFac($res->fakultas_id);
    $scores = $this->Score->fromEmployee("code", $employeeNo, $sem);
    $chart = $this->Score->fromEmployee("code", $employeeNo, $sem);

    if ($res->user_type == '3') {
      //if dosen, group by semester
      $chart = $this->Score->groupByTerm();
    }

    $semesters = $this->general_model->getSemesters();
    $cdata = [];
    $sdata = [];
    $x = 0;

    $subjects = $this->general_model->getSubjects();
    foreach ($subjects as $t) {
      $cdata['series'][$t->subject_id]['data'][$x] = rand(1, 5);
      $cdata['series'][$t->subject_id]['name'] = "'" . $t->subject_name . "'";
    }

    foreach ($semesters as $sem) {
      $sdata[] = "'" . $sem->term_title . " " . $sem->year . "'";

      $this->db->select('*,AVG(NULLIF(score.score,0)) as avg_score');
      $this->db->from('score');
      $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
      $this->db->join('subjects', 'score.subject_id=subjects.subject_id');
      $this->db->where('score.semester_id', $sem->term_ID);
      $this->db->where('score.lecturer_id', $empID);
      $this->db->group_by('score.subject_id');
      $query = $this->db->get();
      $r = $query->result();
      $i = 0;
      foreach ($r as $j) {
        $cdata['series'][$j->subject_id]['name'] = "'" . $j->subject_name . "'";
        $cdata['series'][$j->subject_id]['data'][] = doubleval(number_format($j->avg_score, 2));
        $i++;
      }
      $x++;
    }
    $cdata['categories'] = implode(',', $sdata);
    $cdata['series'] = array_values($cdata['series']);

    $data = array(
        'identity' => $res,
        'prodi' => $prodi,
        'scores' => $scores,
        'chart' => $chart,
        'cdata' => $cdata,
        'semesters' => $semesters
    );
    $this->load->view('fix_template/global_header', $data);
    $this->load->view('allscore_by_lecturer', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function lecturer_score() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');
    $get_employee = $this->db->get();
    $res = $get_employee->result();

    $prodi = $this->general_model->getProdi();

    $data = array(
        'identity' => $res[0],
        'prodi' => $prodi
    );
    $this->load->view('fix_template/global_header', $data);
    $this->load->view('lecturer_score', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function my_score() {

    $empID = $_SESSION['employeeno'];
    $this->load->model('general_model');
    $this->load->model('Score');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $empID . '');
    $get_employee = $this->db->get();
    $res = $get_employee->result();

    $myscores = $this->Score->findMine();
    $semesters = $this->general_model->getSemesters();
    $cdata = [];
    $sdata = [];
    $x = 0;

    $subjects = $this->general_model->getSubjects();
    foreach ($subjects as $t) {
      $cdata['series'][$t->subject_id]['data'][$x] = rand(1, 5);
      $cdata['series'][$t->subject_id]['name'] = "'" . $t->subject_name . "'";
    }

    foreach ($semesters as $sem) {
      $sdata[] = "'" . $sem->term_title . " " . $sem->year . "'";

      $this->db->select('*,AVG(NULLIF(score.score,0)) as avg_score');
      $this->db->from('score');
      $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
      $this->db->join('subjects', 'score.subject_id=subjects.subject_id');
      $this->db->where('score.semester_id', $sem->term_ID);
      $this->db->where('score.lecturer_id', $empID);
      $this->db->group_by('score.lecturer_id');
      $query = $this->db->get();
      $r = $query->result();
      $i = 0;
      foreach ($r as $j) {
        $cdata['series'][$j->subject_id]['name'] = "'" . $j->subject_name . "'";
        $cdata['series'][$j->subject_id]['data'][] = doubleval(number_format($j->avg_score, 2));
        $i++;
      }
      $x++;
    }
    $cdata['categories'] = implode(',', $sdata);
    $cdata['series'] = array_values($cdata['series']);
    $sem = isset($_GET['sem']) ? $_GET['sem'] : null;
    $data = array(
        'identity' => $res[0],
        'myscores' => $myscores,
        'scores' => $this->Score->fromEmployee("code", $empID, $sem),
        'cdata' => $cdata
    );
    $this->load->view('fix_template/global_header', $data);
    $this->load->view('my_score', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function score_linechart_page() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_employee = $this->db->get();
    $employee_result['employee'] = $get_employee->result();

    $data = array(
        'identity' => $employee_result['employee']
    );

//          print_r($data);
//          die;

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('scorelinechart', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function score_graphic_page() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_employee = $this->db->get();
    $employee_result['employee'] = $get_employee->result();



    $data = array(
        'employee_identity' => $employee_result['employee']
    );

    /* testing database
      echo '<pre>';
      print_r($getcontact['mycontact']);
      die();
     */

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('scoregraphic', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  public function score_table_page() {
    $this->load->model('general_model');

    $this->db->select('*');
    $this->db->from('database');
    $this->db->join('position', 'database.id_position = position.post_id');
    $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
    $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
    $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

    $get_employee = $this->db->get();
    $employee_result['employee'] = $get_employee->result();



    $data = array(
        'employee_identity' => $employee_result['employee']
    );

    /* testing database
      echo '<pre>';
      print_r($getcontact['mycontact']);
      die();
     */

    $this->load->view('fix_template/global_header', $data);
    $this->load->view('scoretable', $data);
    $this->load->view('fix_template/global_footer', $data);
    $this->load->view('fix_template/global_foot', $data);
  }

  function debug($v) {
    echo '<pre>';
    print_r($v);
    echo '</pre>';
  }

  function getActualScore($id) {
    $d = $this->Score->findBycode($id);
    $i = 1;
    $total_score_18 = 0;
    foreach ($d as $score) {
      if ($i <= 18) {
        $total_score_18 += $score->score;
      }
      $i++;
    }
    return ($total_score_18 / 18);
  }

}
