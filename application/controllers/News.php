<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class News extends CI_Controller {

      function __construct() {
          parent::__construct();
          date_default_timezone_set("Asia/Bangkok");

          //call database
          $this->load->model('NewsModel');
          $this->load->database();

          if (!$this->session->userdata('employeeno')) {
              redirect(base_url("login_process"));
          }
      }

      function index() {
          $data = [
            'news' => $this->NewsModel->find()
          ];
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('news/index', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function add() {
          $data = [
          ];
          $this->load->helper(array('form', 'url'));
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('news/add', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function create() {

          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
          $this->form_validation->set_rules('news_title', 'Title', 'required');
          $this->form_validation->set_rules('isi_news', 'Isi', 'required');

          if ($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('msg', 'Isian belum lengkap. Pls fix the error(s)');
              redirect('news/add');
          }

          $data = array(
            'news_title' => $this->input->post('news_title'),
            'isi_news' => $this->input->post('isi_news'),
            'staff' => isset($_SESSION['employeeno']) ? $_SESSION['employeeno'] : 1
          );
          $this->db->insert('berita', $data);
          redirect('news/index');
      }

      function setheadline($id) {
          $news = $this->NewsModel->findFirstByid($id);
          if ($news) {
              $others = $this->NewsModel->find();
              foreach ($others as $o) {
                  $dn['headline']='N';
                  $this->db->where("news_id", $o->news_id);
                  $this->db->update("berita", $dn);
              }
              sleep(1);
              $data['headline'] = 'Y';
              $this->db->where("news_id", $id);
              $this->db->update("berita", $data);
              redirect('news/index');
          }
      }

      function edit($id) {
          $data = [
            'news' => $this->NewsModel->findFirstByid($id)
          ];
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('news/edit', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function view($id) {
          $data = [
            'news' => $this->NewsModel->findFirstByid($id)
          ];
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('news/view', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function update() {
          $data = array(
            'news_title' => $this->input->post('title'),
            'isi_news' => $this->input->post('isi')
          );
          $this->db->where('news_id', $this->input->post('id'));
          $this->db->update('berita', $data);
          redirect('news/index');
      }

      public function delete($id) {
          $this->db->where('news_id', $id);
          $this->db->delete('berita');
          redirect('news/index');
      }

// Login 		Login 		Login 		Login 		Login 		
      function masuk() {
          //http://localhost/uph/general/masuk
          redirect(base_url() . 'index');
      }

      function form_validation() {
          $this->load->library('form_validation');
          $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          $this->form_validation->set_rules("Birthday", "Birthday", 'required|alpha');
          #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          if ($this->form_validation->run()) {
              //true  
              $this->load->model("general_model");
              $data = array(
                "EmployeeNo" => $this->input->post("employeeno")
              );
          } else {
              //false  
              $this->masuk();
          }
      }

      public function my_account() {

          $this->load->helper('form');
          $this->load->library('form_validation');
          $this->load->model('general_model');
          $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');
          $get_employee = $this->db->get();
          $res = $get_employee->result();
          $positions = $this->general_model->getPositions();
          $data = array(
            'employee_identity' => $res,
            'positions' => $this->general_model->getPositions(),
            'fakultas' => $this->general_model->getFakultas(),
            'prodi' => $this->general_model->getProdi()
          );
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('myaccount', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function display_account() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();
          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('display', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function contact_us() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->from('contact_us');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_all = $this->db->get();
          $get_contact = $get_all->result();

          $data = array(
            'contact' => $get_contact,
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('contactus', $data);
          $this->load->view('fix_template/global_contact_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

//	Score 		Score 		Score 		Score 		Score 		
      public function all_score_page_kaprodi() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('allscore_kaprodi', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function all_score_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );
          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('allscore', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_linechart_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scorelinechart', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_graphic_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scoregraphic', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_table_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('database');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scoretable', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

  }
  