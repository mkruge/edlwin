<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Kelas extends CI_Controller {

      function __construct() {
          parent::__construct();
          date_default_timezone_set("Asia/Bangkok");

          //call database
          $this->load->model('KelasModel');
          $this->load->model('general_model');
          $this->load->database();

          if (!$this->session->userdata('employeeno')) {
              redirect(base_url("login_process"));
          }
      }

      function index() {

          $this->db->select('*');
          $this->db->from('kelas');
          if (isset($_GET['faculty']) && $_GET['faculty'] != '') {
              $this->db->where('id_fakultas = ' . $_GET['faculty'] . '');
              //jika ada $_GET faculty, maka filter by faculty
          }
          if (isset($_GET['name']) && $_GET['name'] != '') {
              $this->db->where("EmployeeName LIKE '%" . $_GET['name'] . "%'");
              //jika ada $_GET name, maka filter by name of the employee
          }
          $kelas = $this->db->get()->result();

          $data = [
            'kelas' => $kelas,
            'faculties' => $this->general_model->getFakultas()
          ];
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('kelas/index', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function add() {
          $data = [
            'identity' => null,
            'positions' => $this->general_model->getPositions(),
            'fakultas' => $this->general_model->getFakultas(),
            'prodi' => $this->general_model->getProdi()
          ];
          $this->load->helper(array('form', 'url'));
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('kelas/add', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function create() {

          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
          
          $data = array(
            'name' => $this->input->post('name'),
            'code' => $this->input->post('code')
          );
          $this->db->insert('kelas', $data);
          redirect('kelas/index');
      }

      function edit($id) {

          $this->load->helper('form');
          $this->load->library('form_validation');
          $this->load->model('KelasModel');
          $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          $positions = $this->general_model->getPositions();
          $data = array(
            'kelas' => $this->KelasModel->findFirstByid($id),
            'positions' => $this->general_model->getPositions(),
            'fakultas' => $this->general_model->getFakultas(),
            'prodi' => $this->general_model->getProdi()
          );
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('kelas/edit', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      function update() {

          $data = array(
            'name' => $this->input->post('name'),
            'code' => $this->input->post('code')
          );
          $this->db->where('id', $this->input->post('id'));
          $this->db->update('kelas', $data);
          redirect('kelas/index');
      }

      public function delete($id) {
          $this->db->where('id', $id);
          $this->db->delete('kelas');
          redirect('kelas/index');
      }

// Login 		Login 		Login 		Login 		Login 		
      function masuk() {
          //http://localhost/uph/general/masuk
          redirect(base_url() . 'index');
      }

      function form_validation() {
          $this->load->library('form_validation');
          $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          $this->form_validation->set_rules("Birthday", "Birthday", 'required|alpha');
          #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          #$this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          if ($this->form_validation->run()) {
              //true  
              $this->load->model("general_model");
              $data = array(
                "EmployeeNo" => $this->input->post("employeeno")
              );
          } else {
              //false  
              $this->masuk();
          }
      }

      public function my_account() {

          $this->load->helper('form');
          $this->load->library('form_validation');
          $this->load->model('general_model');
          $this->form_validation->set_rules("employeeno", "Username", 'required|alpha');
          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');
          $get_employee = $this->db->get();
          $res = $get_employee->result();
          $positions = $this->general_model->getPositions();
          $data = array(
            'employee_identity' => $res,
            'positions' => $this->general_model->getPositions(),
            'fakultas' => $this->general_model->getFakultas(),
            'prodi' => $this->general_model->getProdi()
          );
          $this->load->view('fix_template/global_header', $data);
          $this->load->view('myaccount', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function display_account() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();
          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('display', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function contact_us() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->from('contact_us');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_all = $this->db->get();
          $get_contact = $get_all->result();

          $data = array(
            'contact' => $get_contact,
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('contactus', $data);
          $this->load->view('fix_template/global_contact_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

//	Score 		Score 		Score 		Score 		Score 		
      public function all_score_page_kaprodi() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('allscore_kaprodi', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function all_score_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );
          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('allscore', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_linechart_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scorelinechart', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_graphic_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scoregraphic', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

      public function score_table_page() {
          $this->load->model('general_model');

          $this->db->select('*');
          $this->db->from('kelas');
          $this->db->join('position', 'database.id_position = position.post_id');
          $this->db->join('fakultas', 'database.id_fakultas = fakultas.fakultas_id');
          $this->db->join('prodi', 'database.id_prodi = prodi.prodi_id');
          $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

          $get_employee = $this->db->get();
          $employee_result['employee'] = $get_employee->result();



          $data = array(
            'employee_identity' => $employee_result['employee']
          );

          /* testing database
            echo '<pre>';
            print_r($getcontact['mycontact']);
            die();
           */

          $this->load->view('fix_template/global_header', $data);
          $this->load->view('scoretable', $data);
          $this->load->view('fix_template/global_footer', $data);
          $this->load->view('fix_template/global_foot', $data);
      }

  }
  