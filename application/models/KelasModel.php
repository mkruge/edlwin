<?php

  /*
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */

  class KelasModel extends CI_Model {

      function find() {
          $this->db->select('*');
          $result = $this->db->get('kelas');
          return $result->result();
      }

      function findFirstBycode($code = null) {
          $this->db->select('*');
          $this->db->where("code", $code);
          $result = $this->db->get('kelas');
          return $result->result()[0];
      }
      
      function findFirstByid($code = null) {
          $this->db->select('*');
          $this->db->where("id", $code);
          $result = $this->db->get('kelas');
          return $result->result()[0];
      }

      function update($data, $code) {
          $this->db->where("code", $code);
          $this->db->update("kelas", $data);
      }

      function add($staff) {
          $data = array(
            'name' => $this->input->post('name'),
            'code' => $this->input->post('code')
          );
          $this->db->insert('kelas', $data);
      }

  }
  