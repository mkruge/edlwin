<?php

class General_model extends CI_Model {

  function select_data() {
    $this->db->select('*');
    $result = $this->db->get('database');

    return $result->result();
  }

  function getSemesters($id = null) {
    $this->db->select('*');
    if ($id != null):
      $this->db->where('term_ID=' . $id);
      $result = $this->db->get('semester');
      $res = $result->result()[0];
    else:
      $result = $this->db->get('semester');
      $res = $result->result();
    endif;
    return $res;
  }

  function getSubjects() {
    $this->db->select('*');
    $result = $this->db->get('subjects');
    return $result->result();
  }

  function getPositions($admin = false) {
    $this->db->select('*');
    if ($admin == false) {
      $this->db->where('post_id !=4');
    }
    $result = $this->db->get('position');
    return $result->result();
  }

  function getFakultas() {
    $this->db->select('*');
    $result = $this->db->get('fakultas');
    return $result->result();
  }

  function getProdi() {
    $this->db->select('*');
    $result = $this->db->get('prodi');
    return $result->result();
  }

  function getProdiByFac($id) {
    $this->db->select('*');
    $this->db->where('faculty_id =' . $id);
    $result = $this->db->get('prodi');
    return $result->result();
  }

  function getTransaksi($id) {
    $this->db->select('*');
    $this->db->from('database');
    $this->db->from('semester');
    $this->db->join('prodi', 'database.id_prodi=prodi.prodi_id');
    $this->db->where('prodi_id =' . $id);

//      $this->db->where('prodi_id = '.$_SESSION['employeeno'].'');

    $query = $this->db->get(); //simpan database yang udah di get alias ambil ke query
    if ($query->num_rows() > 0) { //membuat data masuk ke $data kemudian masuk lagi ke array $hasiltransaksi
      foreach ($query->result() as $data) {
        # code...
        $hasilTransaksi[] = $data;
      }
      return $hasilTransaksi; //hasil dari semua proses ada dimari
    }
  }

  function fetch_data() {
    //$query = $this->db->get("tbl_user");  
    //select * from tbl_user  
    //$query = $this->db->query("SELECT * FROM tbl_user ORDER BY id DESC");  
    $this->db->select("*");
    $this->db->from("database");
    $query = $this->db->get();
    return $query;
  }

  function getEmployeeData($EmployeeNo) {
    $this->db->where("EmployeeNo", $EmployeeNo);
    $result = $this->db->get("database");
    return $result->result()[0];
  }

  function getAllEmployee($facultyId = null, $prodiId = null, $empName = null) {
    $this->db->select("*");
    if ($facultyId != null) {
      $this->db->where("id_fakultas", $facultyId);
    }
    if ($prodiId != null) {
      $this->db->where("id_prodi", $prodiId);
    }
    if ($empName != null) {
      $this->db->like('EmployeeName', 'both');
    }
    $result = $this->db->get("database");
    return $result->result();
  }

  function getAllEmployeeData($array) {
    $this->db->select("*");
    if (isset($array['facultyId'])) {
      $this->db->where("id_fakultas", $array['facultyId']);
    }
    if (isset($array['prodiId'])) {
      $this->db->where("id_prodi", $array['prodiId']);
    }

    if (isset($array['empName'])) {
      $this->db->like('EmployeeName', $array['empName']);
    }
    $result = $this->db->get("database");
    return $result->result();
  }

  function fetch_single_data($EmployeeNo) {
    $this->db->where("EmployeeNo", $EmployeeNo);
    $query = $this->db->get("database");
    return $query;
  }

  function update_data($data, $EmployeeNo) {
    $this->db->where("EmployeeNo", $EmployeeNo);
    $this->db->update("database", $data);
    //UPDATE tbl_user SET first_name = '$first_name', last_name = '$last_name' WHERE id = '$id'  
  }

  function term_data() {
    $this->db->select('*');
    $result = $this->db->get('semester');

    return $result->result();
  }

  function lapor_data() {
    $this->db->select('*');
    $result = $this->db->get('laporan');

    return $result->result();
  }

  function quest_data() {
    $this->db->select('*');
    $result = $this->db->get('pertanyaan');

    return $result->result();
  }

  function prodi_data() {
    $this->db->select('*');
    $result = $this->db->get('prodi');

    return $result->result();
  }

  function can_login($EmployeeNo, $password) {
    $this->db->where('employeeno', $EmployeeNo);
    $this->db->where('password', $password);
    $query = $this->db->get('database');

    if ($query->num_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

}

?>