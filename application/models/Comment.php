<?php

  /*
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */

  class Comment extends CI_Model {

      function find() {
          $this->db->select('*');
          $result = $this->db->get('comments');
          return $result->result();
      }

      function findFirstByid($id = null) {
          $this->db->select('*');
          $this->db->where("id", $id);
          $result = $this->db->get('comments');
          return $result->result()[0];
      }

      function findByScoreId($id = null) {
          $this->db->select('*');
          $this->db->where("score_id", $id);
          $result = $this->db->get('comments');
          return $result->result();
      }

      function update($data, $id) {
          $this->db->where("id", $id);
          $this->db->update("comments", $data);
      }

      function add($data = []) {
          $this->db->insert('comments', $data);
      }

  }
  