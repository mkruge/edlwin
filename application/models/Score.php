<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Score extends CI_Model {

    function getHeader($id) {

        $this->db->select('*');
        $this->db->from('score');
        $this->db->join('semester', 'semester.term_ID=score.semester_id');
        $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
        $this->db->join('subjects', 'subjects.subject_id=score.subject_id');
        $this->db->where("score.code", $id);
        $result = $this->db->get();
        if (count($result->result()) > 0) {
            return $result->result()[0];
        } else {
            return false;
        }
    }

    function getQuestions() {
        $this->db->select('*');
        $this->db->from('pertanyaan');
        $q = $this->db->get();
        $result = $q->result();
        foreach ($result as $s) {
            $res[] = $s->Pertanyaan;
        }
        return $res;
    }

    function find($employeeNo = null, $faculty = null, $prodi = null) {
        $this->db->select('*,AVG(score.score) as avg_score,sum(score.score) as sum_score');
        $this->db->join('semester', 'semester.term_ID=score.semester_id');
        $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
        $this->db->join('prodi', 'database.id_prodi=prodi.prodi_id');
        $this->db->join('fakultas', 'database.id_fakultas=fakultas.fakultas_id');


        if ($employeeNo !== null) {
            $this->db->where("lecturer_id", $employeeNo);
        }
        if ($prodi !== null) {
            $this->db->where("database.id_prodi", $prodi);
        }
        if ($faculty !== null) {
            $this->db->where("prodi.faculty_id", $faculty);
        }
        $this->db->group_by('score.code');
        $result = $this->db->get('score');
        return $result->result();
    }

    function findMine($faculty = null, $prodi = null) {
        $this->db->select('*,AVG(score.score) as avg_score,sum(score.score) as sum_score');
        $this->db->join('semester', 'semester.term_ID=score.semester_id');
        $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
        $this->db->join('prodi', 'database.id_prodi=prodi.prodi_id');
        $this->db->join('fakultas', 'database.id_fakultas=fakultas.fakultas_id');
        $this->db->where("lecturer_id", $_SESSION['employeeno']);

        if ($prodi !== null) {
            $this->db->where("database.id_prodi", $prodi);
        }
        if ($faculty !== null) {
            $this->db->where("prodi.faculty_id", $faculty);
        }
        //$this->db->group_by('semester.term_ID');
        $this->db->group_by('score.code');
        $result = $this->db->get('score');
        return $result->result();
    }

    function groupByTerm($faculty = null, $prodi = null) {
        $this->db->select('*,AVG(score.score) as avg_score,sum(score.score) as sum_score');
        $this->db->join('semester', 'semester.term_ID=score.semester_id');
        $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
        $this->db->join('prodi', 'database.id_prodi=prodi.prodi_id');
        $this->db->join('fakultas', 'database.id_fakultas=fakultas.fakultas_id');
        $this->db->where("score.lecturer_id", $_SESSION['employeeno']);

        if ($prodi !== null) {
            $this->db->where("database.id_prodi", $prodi);
        }
        if ($faculty !== null) {
            $this->db->where("prodi.faculty_id", $faculty);
        }
        $this->db->group_by('semester.term_ID');
        //$this->db->group_by('score.code');
        $result = $this->db->get('score');
        return $result->result();
    }

    function groupByEmployee($faculty = null, $prodi = null) {
        $this->db->select('*,'
                . 'AVG(NULLIF(score.score,0)) as avg_score,'
                . 'AVG(NULLIF(score.information_provider,0)) as avg_information_provider,'
                . 'AVG(NULLIF(score.role_model,0)) as avg_role_model,'
                . 'AVG(NULLIF(score.facilitator,0)) as avg_facilitator,'
                . 'AVG(NULLIF(score.assesor,0)) as avg_assesor,'
                . 'AVG(NULLIF(score.student_learning,0)) as avg_student_learning');
        $this->db->from('score');
        $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
        $this->db->join('prodi', 'database.id_prodi=prodi.prodi_id');
        $this->db->join('fakultas', 'database.id_fakultas=fakultas.fakultas_id');

        if (isset($_GET['prodi']) && $_GET['prodi'] != '') {
            $this->db->where("database.id_prodi", $_GET['prodi']);
        }

        if ($faculty != null) {
            $this->db->where("database.id_fakultas", $faculty);
        }

        $this->db->group_by('database.EmployeeNo');
        //$this->db->group_by('score.code');
        $result = $this->db->get();
        return $result->result();
    }

    function fromEmployee($group = 'code', $employee_id = null, $sem = null) {
        $this->db->select('*,'
                . 'AVG(NULLIF(score.score,0)) as avg_score,'
                . 'AVG(NULLIF(score.information_provider,0)) as avg_information_provider,'
                . 'AVG(NULLIF(score.role_model,0)) as avg_role_model,'
                . 'AVG(NULLIF(score.facilitator,0)) as avg_facilitator,'
                . 'AVG(NULLIF(score.assesor,0)) as avg_assesor,'
                . 'AVG(NULLIF(score.student_learning,0)) as avg_student_learning');
        $this->db->from('score');
        $this->db->join('subjects', 'score.subject_id=subjects.subject_id');
        $this->db->join('semester', 'semester.term_ID=score.semester_id');
        $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
        $this->db->join('prodi', 'database.id_prodi=prodi.prodi_id');
        $this->db->join('fakultas', 'database.id_fakultas=fakultas.fakultas_id');
        $this->db->where('score.question_id <=', 18);
        $this->db->where("score.lecturer_id", $employee_id);
        
        if ($sem != null) {
            $this->db->where("score.semester_id", $sem);
        }

        switch ($group):
            case "semester":
                $this->db->group_by('score.semester_id');
                break;
            case "code":
                $this->db->group_by('score.code');
                break;
            case "prodi":
                $this->db->group_by('prodi.prodi_id');
                break;
            case "faculty":
                $this->db->group_by('fakultas.fakultas_id');
                break;
        endswitch;

        $result = $this->db->get();
        return $result->result();
    }

    function findFromProdi() {
        $this->db->select('*,AVG(NULLIF(score.score,0)) as avg_score,sum(score.score) as sum_score');
        $this->db->from('score');
        $this->db->join('semester', 'semester.term_ID=score.semester_id');
        $this->db->join('database', 'database.EmployeeNo=score.lecturer_id');
        $this->db->join('prodi', 'database.id_prodi=prodi.prodi_id');
        $this->db->join('fakultas', 'database.id_fakultas=fakultas.fakultas_id');
        $this->db->where("score.lecturer_id", $_SESSION['employeeno']);

        if (isset($_GET['prodi']) && $_GET['prodi'] != '') {
            $this->db->where("database.id_prodi", $_GET['prodi']);
        }

        if (isset($_GET['faculty_id']) && $_GET['faculty_id'] != '') {
            $this->db->where("database.id_fakultas", $_GET['faculty_id']);
        }

        $this->db->group_by('database.EmployeeNo');
        $result = $this->db->get();
        return $result->result();
    }

    function findFirstByid($id = null) {
        $this->db->select('*');
        $this->db->where("id", $id);
        $result = $this->db->get('score');
        return $result->result()[0];
    }

    function findBycode($id = null) {
        $this->db->select('*');
        $this->db->where("code", $id);
        $result = $this->db->get('score');
        return $result->result();
    }

    function update($data, $id) {
        $this->db->where("id", $id);
        $this->db->update("score", $data);
    }

    function add($data = []) {
        $this->db->insert('score', $data);
    }

    function saveStudentComment($data = []) {
        $this->db->insert('student_comments', $data);
    }

    function getStudentComment($code) {
        $this->db->select('*');
        $this->db->where("score_code", $code);
        $result = $this->db->get('student_comments');
        if (count($result->result()) > 0) {
            return $result->result()[0];
        } else {
            return false;
        }
    }

}
