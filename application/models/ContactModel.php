<?php

  /*
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */

  class ContactModel extends CI_Model {

      function find() {
          $this->db->select('*');
          $result = $this->db->get('contact_us');
          return $result->result();
      }

      function findFirstByid($id = null) {
          $this->db->select('*');
          $this->db->where("contact_id", $id);
          $result = $this->db->get('contact_us');
          return $result->result()[0];
      }

      function update($data, $id) {
          $this->db->where("id", $id);
          $this->db->update("contact_us", $data);
      }

      function add($staff) {
          $data = array(
            'contact_name' => $this->input->post('contact_name'),
            'contact_position' => $this->input->post('contact_position')
          );
          $this->db->insert('contact_us', $data);
      }

  }
  