<?php

  /*
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */

  class NewsModel extends CI_Model {

      function find() {
          $this->db->select('*');
          $result = $this->db->get('berita');
          return $result->result();
      }

      function findFirstByid($id = null) {
          $this->db->select('*');
          $this->db->where("news_id", $id);
          $result = $this->db->get('berita');
          return $result->result()[0];
      }
      function findFirstbyHeadline() {
          $this->db->select('*');
          $this->db->where("headline","Y");
          $result = $this->db->get('berita');
          return $result->result()[0];
      }
      
      function update($data, $id) {
          $this->db->where("id", $id);
          $this->db->update("berita", $data);
      }

      function add($staff) {
          $data = array(
            'news_title' => $this->input->post('news_title'),
            'isi_news' => $this->input->post('isi_news'),
            'staff' => $staff
          );
          $this->db->insert('berita', $data);
      }

  }
  