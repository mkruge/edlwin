<?php

  /*
   * To change this license header, choose License Headers in Project Properties.
   * To change this template file, choose Tools | Templates
   * and open the template in the editor.
   */

  class DosenModel extends CI_Model {

      function find() {
          $this->db->select('*');
          $result = $this->db->get('database');
          return $result->result();
      }

      function findFirstByid($id = null) {
          $this->db->select('*');
          $this->db->where("EmployeeNo", $id);
          $result = $this->db->get('database');
          return $result->result()[0];
      }

      function update($data, $id) {
          $this->db->where("id", $id);
          $this->db->update("database", $data);
      }

      function add($staff) {
          $data = array(
            'news_title' => $this->input->post('news_title'),
            'isi_news' => $this->input->post('isi_news'),
            'staff' => $staff
          );
          $this->db->insert('database', $data);
      }

  }
  