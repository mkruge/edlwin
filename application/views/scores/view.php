<!-- Main -->
<article id="main">
    <header>
        <h2>Edit</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    
                    <div style="text-align: right; margin-bottom: 10px;">
                        <?php if ($studentComment==true) { ?>
                        <a style="float:left" href="<?php echo base_url(); ?>uploads/student_comments/<?php echo $studentComment->filename; ?>" target="_blank">
                                  <button>Download Student Comment</button>
                              </a>
                          <?php } ?>
                        <?php if ($logindata[0]->user_type == '5') { ?>
                              <a href="<?php echo base_url(); ?>processing_form/reuploadFile?id_lecture=<?php echo $header_scores->EmployeeNo; ?>&sem=<?php echo $header_scores->term_ID ?>&code=<?php echo $header_scores->code ?>">
                                  <button>Re Upload</button>
                              </a>
                              <a href="<?php echo base_url(); ?>scores/upload_comment/<?php echo $header_scores->code; ?>">
                                  <button>Upload Comment</button>
                              </a>
                          <?php } ?>
                    </div>
                    <table class="table table-striped">
                        <tr>
                            <td>
                                <label>Lecturer</label>
                                <?php echo $header_scores->EmployeeName ?>
                            </td>
                            <td>
                                <label>Semester</label>
                                <?php echo $header_scores->term_title ?> <?php echo $header_scores->year ?>
                            </td>
                            <td>
                                <label>Class</label>
                                <?php echo $header_scores->class_id ?>
                            </td>
                            <td>
                                <label>Subject</label>
                                <?php echo $header_scores->subject_name ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td>No</td>
                            <td>Statement</td>
                            <td>Mean</td>
                        </tr>
                        <?php
                          $no = 1;
                          $i = 0;
                          $data = [];
                          $qs = [];
                          $total_score = 0;
                          $total_score_18=0;
                          foreach ($scores as $score):
                              ?>
                              <tr>
                                  <td><?php echo $no; ?></td>
                                  <td><?php echo $questions[$i] ?></td>
                                  <td><?php echo number_format($score->score, 2) ?></td>
                              </tr>
                              <?php
                              $data[] = $score->score;
                              $qs[] = $no;
                              $no++;
                              $i++;
                              $total_score += $score->score;
                              if($i<=18){
                                $total_score_18+= $score->score;
                              }
                              //$total_score = $total_score+$score->score;

                          endforeach;
                        ?>
                        <tr>
                            <td colspan="2">Mean</td>
                            <td>
                                <?php echo number_format($total_score_18 / 18, 4); ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div id="container"></div>
                    <h3>Komentar (<?php echo count($comments); ?>)</h3>
                    <?php
                      if (count($comments) == 0) {
                          echo 'belum ada komentar';
                      } else {
                          ?>
                          <ul>
                              <?php foreach ($comments as $comment) { ?>
                                  <li>
                                      <?php echo $comment->comment; ?>
                                      <br/>
                                      <small><?php echo $comment->created ?></small>
                                  </li>
                              <?php } ?>
                          </ul>
                      <?php }
                    ?>
                    <?php if ($logindata[0]->user_type == '2') { ?>
                          <div style="text-align: center">
                              <hr/>
                              <h4>Send Comment</h4>
                              <form method="post" action="<?php echo base_url(); ?>scores/send_comment">
                                  <input type="hidden" name="score_id" value="<?php echo $header_scores->code ?>"/>
                                  <textarea name="comment" rows="3" cols="100"></textarea>
                                  <p><br/><button>Submit</button></p>
                              </form>
                              <br/><br/>
                              <input type="button" value="Back" href="#" onclick="history.back();">  
                          </div>
                      <?php } else { ?>
                          <p>Login as kaprodi to comment!</p>
                      <?php } ?>
                </section>

            </div>
        </div>
    </section>
</article>
<script type="text/javascript">


    Highcharts.chart('container', {

        chart: {
            type: 'column'
        },
        xAxis: {
            categories: [<?php echo implode(',', $qs) ?>],
            crosshair: true
        },
        title: {
            text: 'Result'
        },

        subtitle: {
            text: ''
        },

        yAxis: {
            min: 0,
            title: {
                text: 'Mean'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
                name: 'Score',
                data: [<?php echo implode(',', $data) ?>]
            }]
    });
</script>


