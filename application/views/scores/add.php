<!-- Main -->
<article id="main">
    <header>
        <h2>Add News</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="post" action="<?php echo base_url(); ?>scores/create">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="scores_title" value="" required="required"/>
                            <?php echo form_error('scores_title'); ?>
                        </div>
                        <div class="form-group">
                            <label>Isi</label>
                            <textarea type="text" class="form-control" name="isi_scores" rows="5" required="required"></textarea>
                             <?php echo form_error('isi_scores'); ?>
                        </div>
                        <div class="form-group">
                            <br/>
                            <button>Submit</button>
                            <input type="button" value="Back" href="#" onclick="history.back();">
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</div>
</article>

