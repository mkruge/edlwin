<!-- Main -->
<article id="main">
    <header>
        <h2>Upload Student Comment</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <div style="text-align: center">
                        <form method="post" action="<?php echo base_url(); ?>scores/upload_comment/<?php echo $header_scores->code ?>" enctype="multipart/form-data">
                            <input type="hidden" name="score_id" value="<?php echo $header_scores->code ?>"/>
                            <input type="file" name="userfile"/>
                            <p><br/><button name="submit">Submit</button></p>
                        </form>
                        <br/><br/>
                        <input type="button" value="Back" href="#" onclick="history.back();">  
                    </div>
                </section>
            </div>
        </div>
    </section>
</article>