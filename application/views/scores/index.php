<!-- Main -->
<article id="main">
    <header>
        <h2>Score</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">

                <section>
                    <form method="get" action="<?php echo base_url(); ?>scores">
                        <table>
                            <td>
                                <select name="prodi" id="prodi">
                                    <option value="">Select Prodi</option>
                                    <?php foreach ($prodies as $p) { ?>
                                          <option value="<?php echo $p->prodi_id; ?>"
                                          <?php
                                          if (isset($_GET['prodi']) && $_GET['prodi'] == $p->prodi_id)
                                              echo 'selected';
                                          else
                                              echo '';
                                          ?>>
                                              <?php echo $p->academicName_prodi; ?></option>
                                      <?php } ?>
                                </select>
                            </td>
                            <td>
                                <button>Search</button>
                            </td>
                            </tr>                        
                        </table>
                    </form>




                    <?php if (count($scores) > 0) { ?>
                          <table class="dataTable">
                              <thead>
                                  <tr>
                                      <th>id</th>
                                      <th>Lecturer</th>
                                      <th>Faculty/Prodi</th>
                                      <th>Semester</th>
                                      <th>Class</th>
                                      <th>Mean</th>
                                      <td class="text-center">Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($scores as $score) { ?>
                                      <tr>
                                          <td><?php echo $score->id; ?></td>
                                          <td><?php echo $score->EmployeeName; ?></td>
                                          <td><?php echo $score->academicName_fakultas . ',' . $score->academicName_prodi; ?></td>                                      
                                          <td><?php echo $score->term_title; ?> <?php echo $score->year; ?></td>
                                          <td><?php echo $score->class_id; ?></td>
                                          <td><?php echo number_format($score->avg_score, 2); ?></td>
                                          <td><a href="<?php echo base_url(); ?>scores/view/<?php echo $score->code; ?>" class="btn btn-primary btn-sm">detail</a></td>
                                      </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                      <?php } else { ?>
                          <p>No data found!</p>
                      <?php } ?>
                    <p>
                        <br/>
                        <input type="button" value="Back" href="#" onclick="location.href = '<?php echo base_url(); ?>general/my_admin'">
                    </p>
                </section>               
            </div>
        </div>
    </section>
</article>

