<!-- Main -->
<article id="main">
    <header>
        <h2>News Management</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <table style="border-style: hidden; border-color: none;">
                        <td>
                            <h3>News</h3>
                        </td>
                        <td style="text-align: right">
                            <a href="<?php echo base_url(); ?>news/add" class="btn btn-default btn-md">
                                <i class="fa fa-plus-square"></i>
                                Add new News
                            </a>
                        </td>
                    </table>

                    <?php if (count($news) > 0) { ?>
                          <table class="table table-striped table-bordered">
                              <tr>
                                  <td>id</td>
                                  <td width="50%">Title</td>
                                  <td>Headline</td>
                                  <td colspan="4" class='text-center'>Action</td>
                              </tr>
                              <?php foreach ($news as $n) { ?>
                                  <tr>
                                      <td><?php echo $n->news_id; ?></td>
                                      <td><?php echo $n->news_title; ?></td>
                                      <td><?php echo $n->headline; ?></td>
                                      <td><a href="<?php echo base_url(); ?>news/setheadline/<?php echo $n->news_id; ?>" class="btn btn-success btn-md">set headline</a></td>
                                      <td><a href="<?php echo base_url(); ?>news/view/<?php echo $n->news_id; ?>" class="btn btn-primary btn-md">detail</a></td>
                                      <td><a href="<?php echo base_url(); ?>news/edit/<?php echo $n->news_id; ?>" class="btn btn-primary btn-md">edit</a></td>
                                      <td><a href="<?php echo base_url(); ?>news/delete/<?php echo $n->news_id; ?>" class="btn btn-danger btn-md">delete</a></td>
                                  </tr>
                              <?php } ?>
                          </table>
                      <?php } else { ?>
                          <p>No data found!</p>
                      <?php } ?>
                    <p>
                        <br/>
                        <input type="button" value="Back" href="#" onclick="location.href = '<?php echo base_url(); ?>general/my_admin'">
                    </p>
                </section>
            </div>
        </div>
    </section>
</article>

