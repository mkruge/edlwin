<!-- Main -->
<article id="main">
    <header>
        <h2>Edit News #<?php echo $news->news_id ?></h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="post" action="<?php echo base_url(); ?>news/update">
                        <div class="form-group">
                            <label>Title</label>
                            <br/>
                            <?php echo $news->news_title ?>
                        </div>
                        <div class="form-group">
                            <label>Isi</label>
                            <br/>
                            <?php echo nl2br($news->isi_news) ?>
                        </div>
                        <div class="form-group">
                            <br/><br/>
                            <input type="button" value="Back" href="#" onclick="history.back();">
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</article>

