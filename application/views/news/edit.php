<!-- Main -->
<article id="main">
    <header>
        <h2>Edit News #<?php echo $news->news_id ?></h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="post" action="<?php echo base_url(); ?>news/update">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" value="<?php echo $news->news_title ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Isi</label>
                            <textarea type="text" class="form-control" name="isi" rows="5"><?php echo $news->isi_news ?></textarea>
                        </div>
                        <div class="form-group">
                            <button>Submit</button>
                            <input type="hidden" class="form-control" name="id" value="<?php echo $news->news_id ?>"/>
                            <input type="button" value="Back" href="#" onclick="history.back();">
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</article>

