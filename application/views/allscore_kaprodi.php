<?php
    $link=mysqli_connect("localhost", "root", "");
    mysqli_select_db($link, "test");
?>

<!-- Main -->
					<article id="main">
						<header>
							<h2>All Score</h2>
						</header>
                                  
						<section class="wrapper style5">
                         
            <div class="templatemo-content-widget no-padding" id="templatemo">
            <div class="panel panel-default table-responsive">
              <table class="table table-striped table-bordered templatemo-user-table">
                <thead >
                  <tr>
                    <th><b>No</b></td>
                    <th><b>Name</b></td>
                    <th><b>Mean</b></td>
                    <th><b>Information</b></td>
                    <th><b>Comment</b></td>
                  </tr>
                </thead>
                
                

            <tbody style="text-align: center;">
                <?php
                    $res=mysqli_query($link, "SELECT * FROM laporan");
                    while($baris=mysqli_fetch_array($res))
                  { ?>
                  <tr>
                    <td style="width: 10%"><?php echo $baris["Lec_ID"]; ?></td>
                    <td><?php echo $baris["LecName"]; ?></td>
                    <td><?php echo $baris["mean_p1_p18"]; ?></td>
                    <td>Good</td>
                    <td><input type="text" name=""></td>
                    <td style="width: 7%"><input type="submit" name="" value="Send"></td>
                  </tr>  
            <?php } ?>
            </tbody>
            </table>
            </div>
            </div>
            <br><br><br>
            
            
            <table style="text-align: center; vertical-align: middle; background-color: #ffffff">
            <tbody style="background-color: #ffffff">
            <td>
              <a href="<?php echo base_url('general/score_linechart_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/line_chart.png') ?>" alt="line-chart" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Line Chart
            </td>

            <td>
              <a href="<?php echo base_url('general/score_graphic_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/graph.png') ?>" alt="graphic" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Bar Graph
            </td>

            <td>
              <a href="<?php echo base_url('general/score_table_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/table.png') ?>" alt="table" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Table
            </td>
            </tbody>
            </table>
					</section>
					</article>