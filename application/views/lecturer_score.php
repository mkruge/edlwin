<!-- Main -->
<article id="main">
    <header>
        <h2>All Score</h2>
    </header>
    <section class="wrapper style5">
        <div class="templatemo-content-widget white-bg col-1 text-center templatemo-position-relative" style="width:25%; margin-left: auto;
             margin-right: auto;" >
            <center><img src="<?php echo base_url('assets/images/person.png') ?>" style="width:70%; vertical-align:middle" alt="Avatar" class="img-circle img-thumbnail margin-bottom-5"></center><br>
            <h2 class="text-uppercase blue-text margin-bottom-5" style="text-align:center; color:#102269"><?php echo $identity->EmployeeName; ?></h2>
            <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><?php echo $identity->EmployeeNo; ?></h5>
            <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><b><?php echo $identity->Positions ?></b></h5>
            <h5 class="text-uppercase margin-bottom-30" style="text-align:center"><?php echo $identity->academicName_prodi ?></h5>
        </div>


        <br><br>
        <div class="12u">
            <center>
                <div class="select-wrapper" style="width: 40%">
                    <select name="demo-category" id="demo-category" onchange="window.location.href='<?php echo base_url();?>general/all_score_page?prodi='+this.value">
                        <option selected="selected">-- Prodi --</option>
                        <?php
                          foreach ($prodi as $p) {
                              ?>
                              <option value="<?php echo $p->prodi_id ?>" <?php echo isset($_GET['prodi'])&&$_GET['prodi']==$p->prodi_id?'selected="selected"':''?>>
                                  <?php echo $p->academicName_prodi; ?>
                              </option>
                          <?php } ?>
                    </select>
                </div>
            </center>
        </div>
        <br><br><br>
        <div id="container"></div>
        <table style="text-align: center; vertical-align: middle; background-color: #ffffff">
            <tbody style="background-color: #ffffff">
            <td>
                <a href="<?php echo base_url('general/score_linechart_page') ?>">
                    <input width="50%" type="image" src="<?php echo base_url('assets/images/line_chart.png') ?>" alt="line-chart" class="img-circle img-thumbnail margin-bottom-5">
                </a>
                <br>
                Line Chart
            </td>

            <td>
                <a href="<?php echo base_url('general/score_graphic_page') ?>">
                    <input width="50%" type="image" src="<?php echo base_url('assets/images/graph.png') ?>" alt="graphic" class="img-circle img-thumbnail margin-bottom-5">
                </a>
                <br>
                Bar Graph
            </td>

            <td>
                <a href="<?php echo base_url('general/score_table_page') ?>">
                    <input width="50%" type="image" src="<?php echo base_url('assets/images/table.png') ?>" alt="table" class="img-circle img-thumbnail margin-bottom-5">
                </a>
                <br>
                Table
            </td>
            </tbody>
        </table>
    </section>
</article>


<script type="text/javascript">

    Highcharts.chart('container', {

        title: {
            text: 'Solar Employment Growth by Sector, 2010-2016'
        },

        subtitle: {
            text: 'Source: thesolarfoundation.com'
        },

        yAxis: {
            title: {
                text: 'Score'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                pointStart: 2010
            }
        },

        series: [{
                name: 'Installation',
                data: [4.3934, 5.2503, 5.7177, 5.9658, 4.7031, 4.9931, 3.7133, 5.4175]
            }, {
                name: 'Manufacturing',
                data: [2.4916, 2.4064, 2.9742, 2.9851, 3.2490, 3.0282, 3.8121, 4.0434]
            }, {
                name: 'Sales & Distribution',
                data: [4.744, 4.722, 4.005, 4.771, 3.185, 4.377, 3.147, 3.387]
            }, {
                name: 'Project Development',
                data: [null, null, 4.88, 3.2169, 5.112, 4.52, 3.4400, 3.4227]
            }, {
                name: 'Other',
                data: [4.2908, 5.948, 3.105, 3.248, 3.989, 2.1816, 4.274, 3.111]
            }]

    });

</script>