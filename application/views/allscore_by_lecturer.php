<!-- Main -->
<article id="main">
    <header>
        <h2>Dosen: <br/><?php echo $identity->EmployeeNo . ' ' . $identity->EmployeeName ?></h2>
    </header>
    <section class="wrapper style5">

        <div class="12u">
            <center>
                <div class="select-wrapper" style="width: 40%">
                    <select name="demo-category" id="demo-category" onchange="window.location.href = '<?php echo base_url(); ?>general/all_score_by_lecturer/<?php echo $identity->EmployeeNo ?>?sem=' + this.value">
                        <option value="" selected="selected">-- Semester --</option>
                        <?php
                          foreach ($semesters as $sem) {
                              ?>
                              <option value="<?php echo $sem->term_ID ?>" <?php echo isset($_GET['sem']) && $_GET['sem'] == $sem->term_ID ? 'selected="selected"' : '' ?>>
                                  <?php echo $sem->term_title; ?> <?php echo $sem->year; ?>
                              </option>
                          <?php } ?>
                    </select>
                </div>
            </center>
        </div>
        <br>
        <br>
        <div class="col12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Semester</td>
                        <td>Mean</td>
                        <td>View</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      $chart_data = [];
                      $scoress = [];
                      foreach ($scores as $s) {
                          $chart_data['cat'][] = "'" . $s->EmployeeNo . "/" . $s->EmployeeName . "'";
                          $scoress[] = number_format($s->avg_score, 4);
                          ?>
                          <tr>
                              <td><?php echo $s->term_title ?> <?php echo $s->year ?></td>
                              <td><?php echo $s->subject_code ?>- <?php echo $s->subject_name ?></td>
                              <td><?php echo number_format($s->avg_score, 2) ?></td>
                              <td><a target="_blank" href="<?php echo base_url(); ?>scores/view/<?php echo $s->code ?>">Detail</a></td>
                          </tr>
                          <?php
                      }
                      if (count($scores) > 0) {
                          $chart_data['category'] = implode(',', $chart_data['cat']);
                          $chart_data['series'] = implode(',', $scoress);
                      }
                    ?>
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <div id="container"></div>
        <br/>
        <hr/>
        <br/>
        <div id="container2"></div>

    </section>
</article>
<script type="text/javascript">
    Highcharts.chart('container', {

        title: {
            text: 'Score Per semester'
        },
        subtitle: {
            text: ''
        },
        yAxis: {
            title: {
                text: 'Score Mean'
            }
        },
        xAxis: {
            categories: [<?php echo $cdata['categories'] ?>],
            crosshair: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        series: <?php echo json_encode($cdata['series']); ?>

    });
</script>