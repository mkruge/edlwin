<?php
  foreach ($employee_identity as $identity)
      
      ?>

<!-- Main -->
<article id="main">
    <header>
        <h2>My Account</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">


            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <h2 style="font-size: 25pt">My Account</h2>
                    <form method="post" action="my_account">
                        <div class="row uniform">
                            <div class="6u 12u$(xsmall)">
                                <h4>Full Employee Name</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p>
                                    <?php echo $identity->GelarDepan ?> 
                                    <?php echo $identity->EmployeeName ?> 
                                    <?php echo $identity->GelarBelakang ?>
                                </p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Employee ID</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->EmployeeNo ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Gender</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php
                                      if ($identity->Gender == 0) {
                                          echo 'Female';
                                      } else {
                                          echo 'Male';
                                      }
                                    ?></p>
                            </div>


                            <div class="6u 12u$(xsmall)">
                            </div>
                            <div class="6u 12u$(xsmall)">
                            </div>
                            <div class="6u 12u$(xsmall)">
                            </div>
                            <div class="6u 12u$(xsmall)">
                            </div>


                            <div class="6u 12u$(xsmall)">
                                <h4>Join Date</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->JoinDate ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>NIDN</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->NIDN ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Faculty</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->academicName_fakultas ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Program of Study</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->academicName_prodi ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Positions</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->Positions ?></p>
                            </div>


                            <div class="6u 12u$(xsmall)">
                            </div>
                            <div class="6u 12u$(xsmall)">
                            </div>
                            <div class="6u 12u$(xsmall)">
                            </div>
                            <div class="6u 12u$(xsmall)">
                            </div>



                            <div class="6u 12u$(xsmall)">
                                <h4>Password</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->password ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Birthdate</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->Birthday ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Birthplace</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->Birthplace ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Email</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p><?php echo $identity->Email ?></p>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <h4>Phone Number</h4>
                            </div>
                            <div class="6u 12u$(xsmall)">
                                <p>0<?php echo $identity->phone ?></p>
                            </div>
                            <div class="12u$">
                                <button type="button" onclick="window.location.href='<?php echo base_url(); ?>general/my_account'">
                                  Update
                                </button>
                                <button type="button" onclick="history.back(-1);">Back</button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
    </section>
</div>
</article>