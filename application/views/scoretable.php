<?php
    $link=mysqli_connect("localhost", "root", "");
    mysqli_select_db($link, "test");
?>
<?php
    foreach ($employee_identity as $identity) 
?>

<!-- Main -->
					<article id="main">
						<header>
							<h2>Table Score</h2>
						</header>


              <section class="wrapper style5">
              <div class="templatemo-content-widget white-bg col-1 text-center templatemo-position-relative" style="width:25%; margin-left: auto; margin-right: auto;" >
              <center><img src="<?php echo base_url('assets/images/person.png') ?>" style="width:70%; vertical-align:middle" alt="Bicycle" class="img-circle img-thumbnail margin-bottom-5"></center><br>

              <h2 class="text-uppercase blue-text margin-bottom-5" style="text-align:center; color:#102269"><?php echo $identity->EmployeeName; ?></h2>
              <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><?php echo $identity->EmployeeNo; ?></h5>
              <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><b><?php echo $identity->Positions; ?></b></h5>
              <h5 class="text-uppercase margin-bottom-30" style="text-align:center"><?php echo $identity->academicName_prodi; ?></h5>
            </div>
            <br>
            
                           
            <div class="templatemo-content-widget no-padding" id="templatemo">
            <div class="panel panel-default table-responsive">
              <table class="table table-striped table-bordered templatemo-user-table">
                <thead >
                  <tr>
                    <th><b>No</b></td>
                    <th><b>Question</b></td>
                    <th><b>Mean</b></td>
                    <th><b>Information</b></td>
                  </tr>
                </thead>
                
                

            <tbody>
                <?php
                    $res=mysqli_query($link, "SELECT * FROM laporan");
                    while($baris=mysqli_fetch_array($res))
                  { ?>
                    <?php
                        $res=mysqli_query($link, "SELECT * FROM pertanyaan");
                        while($row=mysqli_fetch_array($res))
                      { ?>
                  <tr>
                    <td><?php echo $row["No"]; ?></td>
                    <td><?php echo $row["Pertanyaan"]; ?></td>
<!--                    <td><?php echo $baris["p1"]; ?></td>    -->
                    <td><?php echo $baris["mean_p1_p18"]; ?></td>
                    <td style="color:#0C0"><b>Good</b></td>
                  </tr>  

            <?php } ?>
            <?php } ?>
            </tbody>
            </table>
            </div>
            </div>

            <br><br><br>



<!--
            <div class="templatemo-content-widget no-padding" id="templatemo">
            <div class="panel panel-default table-responsive">
              <table class="table table-striped table-bordered templatemo-user-table">
                <thead style="text-align: left;">
                  <tr>
                    <th><b>No</b></td>
                    <th><b>Question</b><span class="caret"></span></td>
                    <th><b>Mean</b><span class="caret"></span></td>
                    <th><b>Information</b><span class="caret"></span></td>
                  </tr>
                </thead>
                
                <?php foreach ($lapor as $report) { ?>
                <?php foreach ($quest as $tanya) { ?>
                <tbody>
                  <tr>
                    <td><?php echo $tanya->No; ?></td>
                    <td><?php echo $tanya->Pertanyaan; ?></td>
                    <td><?php echo $report->mean_p1_p18; ?></td>
                    <td style="color:#0C0"><b>Good</b></td>
                  </tr>  
            </tbody>
            <?php } ?>
            <?php } ?>
            </table>
            </div>
            </div>

            <br><br><br>

-->

            <table style="text-align: center; vertical-align: middle; background-color: #ffffff">
            <tbody style="background-color: #ffffff">
            <td>
              <a href="<?php echo base_url('general/score_linechart_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/line_chart.png') ?>" alt="line-chart" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Line Chart
            </td>

            <td>
              <a href="<?php echo base_url('general/score_graphic_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/graph.png') ?>" alt="graphic" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Bar Graph
            </td>

            <td>
              <a href="<?php echo base_url('general/score_table_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/table.png') ?>" alt="table" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Table
            </td>
            </tbody>
            </table>
          </section>
          </article>