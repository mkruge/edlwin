<!-- Main -->
<article id="main">
    <header>
        <h2>Lecturers Management</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="get" action="<?php echo base_url(); ?>dosen/index">
                        <table style="border-style: hidden; border-color: none;">
                            <td>
                                <input type="text" name="name" placeholder="Enter Name" value="<?php echo (isset($_GET['name']) && $_GET['name'] != '') ? $_GET['name'] : ''; ?>"/>
                            </td>
                            <td>
                                <select name="faculty">
                                    <option value="">Select Faculty</option>
                                    <?php foreach ($faculties as $f) { ?>
                                          <option value="<?php echo $f->fakultas_id; ?>" <?php echo (isset($_GET['faculty']) && $_GET['faculty'] == $f->fakultas_id) ? 'selected="selected"' : ''; ?>>
                                              <?php echo $f->academicName_fakultas ?>
                                          </option>
                                      <?php } ?>
                                </select>
                            </td>
                            <td>
                                <button name="search"><i class="fa fa-search"></i> Search</button>
                            </td>
                            <td>
                                <button type="button" onclick="location.href = '<?php echo base_url(); ?>dosen/'">view all</button>
                            </td>
                            <td style="text-align: right">
                                <a href="<?php echo base_url(); ?>dosen/add" class="btn btn-default btn-md">
                                    <i class="fa fa-plus-square"></i>
                                    Add Lecturer
                                </a>
                            </td>
                        </table>
                    </form>
                    <?php if (count($dosen) == 0) { ?>
                          <div class="alert alert-danger">
                              No data found!
                          </div>
                      <?php } else { ?>
                          <table class="dataTable">
                              <thead>
                                  <tr>
                                      <th>EmployeeNo</th>
                                      <th>NIDN</th>
                                      <th>EmployeeName</th>
                                      <th></th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($dosen as $n) { ?>
                                      <tr>
                                          <td><?php echo $n->EmployeeNo; ?></td>
                                          <td><?php echo $n->NIDN; ?></td>
                                          <td><?php echo $n->GelarDepan; ?> <?php echo $n->EmployeeName; ?>, <?php echo $n->GelarBelakang; ?></td>
                                          <td width="3%"><a href="<?php echo base_url(); ?>dosen/edit/<?php echo $n->EmployeeNo; ?>" class="btn btn-primary btn-md">edit</a></td>
                                          <td width="3%"><a href="<?php echo base_url(); ?>dosen/delete/<?php echo $n->EmployeeNo; ?>" class="btn btn-danger btn-md">delete</a></td>
                                      </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                      <?php } ?>
                    <br/>
                    <br/>
                    <input type="button" value="Back" href="#" onclick="location.href = '<?php echo base_url(); ?>general/my_admin'">
                </section>
            </div>
        </div>
    </section>
</article>

