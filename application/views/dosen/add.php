<!-- Main -->
<article id="main">
    <header>
        <h2>Edit Lecturer</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="post" class="validate" action="<?php echo base_url('dosen/create') ?>">
                        <div class="row uniform">
                            <div class="6u 12u">
                                <b>Employee Full Name</b>
                                <input type="text" name="EmployeeName" id="EmployeeName" value="" placeholder="Employee Name" />
                            </div>
                            <div class="6u 12u">
                                <b>Employee ID</b>
                                <input type="text" name="EmployeeNo" id="EmployeeNo" value="" placeholder="Lecturer ID" />
                            </div>

                            <div class="6u 12u">
                                <b>Gelar Depan</b>
                                <input type="text" name="gelardepan" id="gelardepan" value="" placeholder="Gelar Depan" />
                            </div>
                            <div class="6u 12u">
                                <b>Gelar Belakang</b>
                                <input type="text" name="gelarbelakang" id="gelarbelakang" value="" placeholder="Gelar Belakang" />
                            </div>
                            <div class="6u 12u">
                                <b>NIDN</b>
                                <input type="text" name="nidn" id="nidn" value="" placeholder="NIDN" />
                            </div>
                            <div class="6u$ 12u">
                                <b>Birthdate (YYYY-MM-DD)</b>
                                <input type="text" name="Birthday" id="Birthday" value="" placeholder="Birthday" />
                            </div>
                            <div class="6u 12u">
                                <b>Birthplace</b>
                                <input type="text" class="required" name="Birthplace" id="Birthplace" value="" placeholder="Birthplace" />
                            </div>
                            <div class="6u 12u">
                                <b>Email</b>
                                <input type="email" name="email" id="email" value="" placeholder="Email" />
                            </div>
                            <div class="6u 12u">
                                <b>Phone Number</b>
                                <input type="text" name="phone" id="phone" value="" placeholder="Handphone Number" />
                            </div>
                            <div class="6u$ 12u">
                                <b>Join Date</b>
                                <input type="text" name="JoinDate" id="JoinDate" value="" placeholder="Join Date" />
                            </div>
                            <div class="6u$ 12u">
                                <b>Password</b>
                                <input type="text" name="password" id="password" value="" placeholder="Password" />
                            </div>



                            <div>                          
                                <b>Gender</b>        
                                <br>            
                                <div class="4u 12u$(small)" style="width:15%">
                                    <input type="radio" id="demo-priority-low" name="gender" value="1">
                                    <label for="demo-priority-low">Male</label>
                                </div>
                                <div class="4u 12u$(small)" style="width:15%">
                                    <input type="radio" id="demo-priority-normal" name="gender" value="0">
                                    <label for="demo-priority-normal">Female</label>
                                </div>
                            </div>


                            <div class="12u">
                                <div class="select-wrapper">
                                    <select name="position" id="position">
                                        <?php
                                          foreach ($positions as $position) {
                                              ?>
                                              <option value="<?php echo $position->post_id; ?>">
                                                  <?php echo $position->Positions; ?>
                                              </option>
                                          <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="12u">
                                <div class="select-wrapper">
                                    <select name="fakultas" id="fakultas">
                                       <?php
                                          foreach ($fakultas as $f) {
                                              ?>
                                              <option value="<?php echo $f->fakultas_id; ?>"><?php echo $f->academicName_fakultas; ?></option>
                                          <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="12u">
                                <div class="select-wrapper">
                                    <select name="prodi" id="prodi">
                                       <?php
                                          foreach ($prodi as $p) {
                                              ?>  
                                              <option value="<?php echo $p->prodi_id; ?>"><?php echo $p->academicName_prodi; ?></option>
                                          <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="12u">
                                <ul class="actions">
                                    <li><button type="submit" value="Save">Save</button></li>
                                    <li><input type="button" value="Back" href="#" onclick="history.back();"></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</article>

