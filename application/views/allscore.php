<!-- Main -->
<article id="main">
    <header>
        <h2>All Score</h2>
    </header>
    <section class="wrapper style5">
        <div class="templatemo-content-widget white-bg col-1 text-center templatemo-position-relative" style="width:25%; margin-left: auto;
             margin-right: auto;" >
            <center><img src="<?php echo base_url('assets/images/person.png') ?>" style="width:70%; vertical-align:middle" alt="Avatar" class="img-circle img-thumbnail margin-bottom-5"></center><br>
            <h2 class="text-uppercase blue-text margin-bottom-5" style="text-align:center; color:#102269"><?php echo $identity->EmployeeName; ?></h2>
            <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><?php echo $identity->EmployeeNo; ?></h5>
            <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><b><?php echo $identity->Positions ?></b></h5>
            <h5 class="text-uppercase margin-bottom-30" style="text-align:center"><?php echo $identity->academicName_prodi ?></h5>
        </div>
        <?php if ($identity->user_type == '1' || $identity->user_type == '5') { ?>
          <br><br>
          <div class="12u" style="text-align:center;">
                  <form method="get" action="<?php echo base_url(); ?>general/all_score_page">
                      <div class="select-wrapper" style="display:inline-block;width: 30%">     
                          <input type="text" name="nama_dosen" placeholder="nama dosen" value="<?php echo isset($_GET['nama_dosen'])?$_GET['nama_dosen']:''?>"/>
                      </div>
                      <div class="select-wrapper" style="display:inline-block;width: 30%">                          
                          <select name="prodi" id="demo-category">
                              <option value="" selected="selected">-- Prodi --</option>
                              <?php
                              foreach ($prodi as $p) {
                                ?>
                                <option value="<?php echo $p->prodi_id ?>" <?php echo isset($_GET['prodi']) && $_GET['prodi'] == $p->prodi_id ? 'selected="selected"' : '' ?>>
                                    <?php echo $p->academicName_prodi; ?>
                                </option>
                              <?php } ?>
                          </select>                         
                      </div>
                      <div style="display:inline-block;width: 40%">
                          <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                  </form>
          </div>
        <?php } ?>
        <br>
        <br>        
        <div class="12u">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td width="5%">Lecturer name</td>
                        <td width="5%">Lecturer ID</td>
                        <td>Class</td>
                        <td>Subject</td>
                        <td>No of Students</td>
                        <td>Informatiion Provider (1-7)</td>
                        <td>Role Model (8-12)</td>
                        <td>Facilitator</td>
                        <td>Assesor</td>
                        <td>Student Learning</td>
                        <td>Mean</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $d) { ?>
                      <tr style="font-weight:bold; text-transform:uppercase;">
                          <td>
                              <a href="<?php echo base_url(); ?>general/all_score_by_lecturer/<?php echo $d->EmployeeNo ?>"><?php echo $d->EmployeeName ?></a></td>
                          <td><?php echo $d->EmployeeNo ?></td>
                          <td colspan="9" align="right">
                              <?php echo number_format($d->totalScore, 2) ?>
                          </td>
                      </tr>
                      <?php foreach ($d->data as $ddt) { ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><a href="<?php echo base_url(); ?>scores/view/<?php echo $ddt->code ?>"><?php echo $ddt->class_id ?></a></td>
                            <td><?php echo $ddt->subject_name ?></td>
                            <td><?php echo count(json_decode($ddt->file, true)[0]) ?></td>
                            <td><?php echo number_format($ddt->avg_information_provider, 2) ?></td>
                            <td><?php echo number_format($ddt->avg_role_model, 2) ?></td>
                            <td><?php echo number_format($ddt->avg_facilitator, 2) ?></td>
                            <td><?php echo number_format($ddt->avg_assesor, 2) ?></td>
                            <td><?php echo number_format($ddt->avg_student_learning, 2) ?></td>
                            <td><?php echo number_format($ddt->avg_score, 2) ?></td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <div id="container"></div>
        <br/>
        <hr/>
        <br/>
        <div id="container2"></div>

    </section>
</article>

<script type="text/javascript">

  Highcharts.chart('container', {

      chart: {
          type: 'column'
      },
      title: {
          text: 'Score By Lecturer'
      },
      subtitle: {
          text: ''
      },
      xAxis: {
          categories: [<?php echo $chart_data['category'] ?>],
          crosshair: true
      },
      yAxis: {
          title: {
              text: 'Score'
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [
          {
              name: 'Score',
              data: [<?php echo $chart_data['series'] ?>]
          }
      ]

  });

  Highcharts.chart('container2', {

      title: {
          text: 'Score By Lecturer each semester'
      },
      subtitle: {
          text: ''
      },
      yAxis: {
          title: {
              text: 'Score Mean'
          }
      },
      xAxis: {
          categories: [<?php echo $cdata['categories'] ?>],
          crosshair: true
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },
      series: <?php echo json_encode($cdata['series']); ?>

  });
</script>