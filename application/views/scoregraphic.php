<!-- Main -->
					<article id="main">
						<header>
							<h2>Bar Graph Score</h2>
						</header>
                        
                        
						<section class="wrapper style5">
                        <div class="templatemo-content-widget white-bg col-1 text-center templatemo-position-relative" style="width:25%; margin-left: auto;
    margin-right: auto;" >
              <center><img src="<?php echo base_url('assets/images/person.png') ?>" style="width:70%; vertical-align:middle" alt="Bicycle" class="img-circle img-thumbnail margin-bottom-5"></center><br>

              <?php foreach ($data as $key) { ?>
              <h2 class="text-uppercase blue-text margin-bottom-5" style="text-align:center; color:#102269"><?php echo $key->EmployeeName; ?></h2>
              <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><?php echo $key->EmployeeNo; ?></h5>
              <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><b><?php echo $key->JobStatus; ?></b></h5>
              <h5 class="text-uppercase margin-bottom-30" style="text-align:center"><?php echo $key->ProgramOfStudy; ?></h5>
              <?php } ?>
            </div>



            <br><br><br>

            <div class="templatemo-content-widget no-padding" id="templatemo">
            <div id="container"></div>
            </div>

            <br><br><br>
            




            <table style="text-align: center; vertical-align: middle; background-color: #ffffff">
            <tbody style="background-color: #ffffff">
            <td>
              <a href="<?php echo base_url('general/score_linechart_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/line_chart.png') ?>" alt="line-chart" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Line Chart
            </td>

            <td>
              <a href="<?php echo base_url('general/score_graphic_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/graph.png') ?>" alt="graphic" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Bar Graph
            </td>

            <td>
              <a href="<?php echo base_url('general/score_table_page')?>">
              <input width="50%" type="image" src="<?php echo base_url('assets/images/table.png') ?>" alt="table" class="img-circle img-thumbnail margin-bottom-5">
              </a>
              <br>
              Table
            </td>
            </tbody>
            </table>
						</section>
					</article>


<script type="text/javascript">
    Highcharts.chart('container', {
    title: {
        text: 'Combination chart'
    },
    xAxis: {
        categories: ['Apples', 'Oranges', 'Pears', 'Bananas', 'Plums']
    },
    labels: {
        items: [{
            html: 'Total fruit consumption',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Jane',
        data: [3, 2, 1, 3, 4]
    }, {
        type: 'column',
        name: 'John',
        data: [2, 3, 5, 7, 6]
    }, {
        type: 'column',
        name: 'Eldwin',
        data: [2, 4, 3, 5, 3]
    },{
        type: 'column',
        name: 'Joe',
        data: [4, 3, 3, 9, 1]
    }, {
        type: 'spline',
        name: 'Average',
        data: [3, 2.67, 3, 6.33, 3.33],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, {
        type: 'pie',
        name: 'Total consumption',
        data: [{
            name: 'Jane',
            y: 13,
            color: Highcharts.getOptions().colors[0] // Jane's color
        }, {
            name: 'John',
            y: 23,
            color: Highcharts.getOptions().colors[1] // John's color
        },{
            name: 'Eldwin',
            y: 20,
            color: Highcharts.getOptions().colors[2] // John's color
        }, {
            name: 'Joe',
            y: 19,
            color: Highcharts.getOptions().colors[3] // Joe's color
        }],
        center: [100, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});


</script>