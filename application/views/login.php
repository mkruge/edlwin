<!-- Banner -->
<section id="banner">
    <div class="inner">
        <h2>Welcome</h2><br />
        <img src="<?php echo base_url('assets/images/LogoUPH(BackgroundGelap).png') ?>" class="img-contact"  width="300" height="115" alt="UPH" />
    </div>
    <a href="#one" class="more scrolly">Please Log In</a>
</section>

<!-- One -->
<section id="one" class="wrapper style2 special">
    <div class="inner">
        <header class="major">
            <center>                
                <h3>Please Log In</h3>

                <div class="templatemo-content-widget no-padding" id="templatemo">                   
                    <div style="height: 174px; width: 270px;" >
                        <form  class="form-signin" name="login" method="post" action="<?php echo base_url('login_process/login_validation'); ?>">
                            <div>
                                <label for="lecturerid" class="sr-only">Lecturer ID</label>
                                <input type="text" id="lecturerid" name="employeeno" class="form-control" placeholder="Lecturer ID" required>
                                <span class="text-danger"><?php echo form_error("employeeno"); ?></span>
                            </div>


                            <div style="margin-top: 30px">
                                <label for="inputPassword" class="sr-only">Password</label>
                                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
                                <span class="text-danger"><?php echo form_error("password"); ?></span>
                            </div>

                            <div style="margin-top: 30px">
                                <input type="submit" name="insert" value="Login">
                                <br>
                                <div style="margin: auto; font-size: 12px; text-align: center; color: #f00;">
                                    <?php
                                      echo $this->session->flashdata("error");
                                    ?>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </center>
    </div>
</section>