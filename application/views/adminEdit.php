<!-- Main -->
<article id="main">
    <header>
        <h2>Hello, Admin</h2>
    </header>
    <section class="wrapper style5" style="padding: 1em 0 1em 0;">
        <div class="inner">
            <br><br>
            <center>
                <div class="12u$">
                    <p>Please choose program of study that you want to see</p>
                </div>
                <form method="get" action="<?php echo base_url('login_process/editscore_button') ?>">
                    <div class="12u$" style="width: 50%">
                        <div class="select-wrapper">

                            <select name="subject_lecture" id="demo-category">
                                <option default selected="selected">-- Program of Study --</option>
                                <?php
                                  foreach ($prodi as $p) {
                                      ?>  
                                      <option value="<?php echo $p->prodi_id; ?>">
                                          <?php echo $p->academicName_prodi; ?>
                                      </option>
                                  <?php } ?>
                            </select>
                        </div>

                        <br>
                        <div class="12u$">
                            <ul class="actions">
                                <li><input type="submit" value="Next"/></li>
                            </ul>
                        </div>
                    </div>
                </form>
                <a href="<?php echo base_url('general/my_admin'); ?>" style="padding-left: 10pt" ><button class="special">Admin Menu</button></a>

        </div><br><br><br><br><br>
        </center>
    </section>

</div>
</section>



</div>
</section>
</div>
</article>
