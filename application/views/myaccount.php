<?php
  $identity = $employee_identity;
?>
<!-- Main -->
<article id="main">
    <header>
        <h2>My Account</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <h3>Update My Account</h3>
                    <form method="post" class="validate" action="<?php echo base_url('processing_form/edit_my_account') ?>">
                        <div class="row uniform">
                            <div class="6u 12u">
                                <b>Employee Full Name</b>
                                <input type="text" name="EmployeeName" id="EmployeeName" value="<?php echo $identity->EmployeeName ?>" placeholder="Employee Name" />
                            </div>
                            <div class="6u 12u">
                                <b>Employee ID</b>
                                <input type="text" name="EmployeeNo" id="EmployeeNo" value="<?php echo $identity->EmployeeNo ?>" placeholder="Lecturer ID" />
                            </div>

                            <div class="6u 12u">
                                <b>Gelar Depan</b>
                                <input type="text" name="gelardepan" id="gelardepan" value="<?php echo $identity->GelarDepan ?>" placeholder="Gelar Depan" />
                            </div>
                            <div class="6u 12u">
                                <b>Gelar Belakang</b>
                                <input type="text" name="gelarbelakang" id="gelarbelakang" value="<?php echo $identity->GelarBelakang ?>" placeholder="Gelar Belakang" />
                            </div>
                            <div class="6u 12u">
                                <b>NIDN</b>
                                <input type="text" name="nidn" id="nidn" value="<?php echo $identity->NIDN ?>" placeholder="NIDN" />
                            </div>
                            <div class="6u$ 12u">
                                <b>Birthdate (YYYY-MM-DD)</b>
                                <input type="text" class="datepicker" name="Birthday" id="Birthday" value="<?php echo $identity->Birthday ?>" placeholder="Birthday" />
                            </div>
                            <div class="6u 12u">
                                <b>Birthplace</b>
                                <input type="text" class="required" name="Birthplace" id="Birthplace" value="<?php echo $identity->Birthplace ?>" placeholder="Birthplace" />
                            </div>
                            <div class="6u 12u">
                                <b>Email</b>
                                <input type="email" name="email" id="email" value="<?php echo $identity->Email ?>" placeholder="Email" />
                            </div>
                            <div class="6u 12u">
                                <b>Phone Number</b>
                                <input type="text" name="phone" id="phone" value="<?php echo $identity->phone ?>" placeholder="Handphone Number" />
                            </div>
                            <div class="6u$ 12u">
                                <b>Join Date</b>
                                <input type="text" class="datepicker" name="JoinDate" id="JoinDate" value="<?php echo $identity->JoinDate ?>" placeholder="Join Date" />
                            </div>
                            <div class="6u$ 12u">
                                <b>Password</b>
                                <input type="text" name="password" id="password" value="<?php echo $identity->password ?>" placeholder="Password" />
                            </div>
                            <div>                          
                                <b>Gender</b>        
                                <br>            
                                <div class="4u 12u$(small)" style="width:15%">
                                    <input type="radio" id="demo-priority-low" name="gender" value="1" <?php
                                      if ($identity->Gender == 1) {
                                          echo 'checked';
                                      } else {
                                          
                                      }
                                    ?>>
                                    <label for="demo-priority-low">Male</label>
                                </div>
                                <div class="4u 12u$(small)" style="width:15%">
                                    <input type="radio" id="demo-priority-normal" name="gender" value="0"  <?php
                                      if ($identity->Gender == 0) {
                                          echo 'checked';
                                      } else {
                                          
                                      }
                                    ?> >
                                    <label for="demo-priority-normal">Female</label>
                                </div>
                            </div>


                            <div class="12u">
                                <div class="select-wrapper">
                                    <select name="position" id="position">
                                        <option default selected="selected" value="<?php echo $identity->post_id ?>"><?php echo $identity->Positions ?> (Current)</option>
                                        <?php
                                          foreach ($positions as $position) {
                                              ?>
                                              <option value="<?php echo $position->post_id; ?>">
                                                  <?php echo $position->Positions; ?>
                                              </option>
                                          <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="12u">
                                <div class="select-wrapper">
                                    <select name="fakultas" id="fakultas">
                                        <option default selected="selected" value="<?php echo $identity->fakultas_id ?>"><?php echo $identity->academicName_fakultas ?> (Current) </option>
                                        <?php
                                          foreach ($fakultas as $f) {
                                              ?>
                                              <option value="<?php echo $f->fakultas_id; ?>"><?php echo $f->academicName_fakultas; ?></option>
                                          <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="12u">
                                <div class="select-wrapper">
                                    <select name="prodi" id="prodi">
                                        <option default selected="selected" value="<?php echo $identity->prodi_id ?>"><?php echo $identity->academicName_prodi ?> (Current) </option>
                                        <?php
                                          foreach ($prodi as $p) {
                                              ?>  
                                              <option value="<?php echo $p->prodi_id; ?>"><?php echo $p->academicName_prodi; ?></option>
                                          <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="12u">
                                <ul class="actions">
                                    <li><button type="submit" value="Save">Save</button></li>
                                    <li><input type="button" value="Back" href="#" onclick="history.back();"></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</div>
</article>
<!-- 
        <div class="12u">
        <div class="select-wrapper">
        <select name="demo-category" id="demo-category">
             <option default selected="selected">-- Position --</option>
             <option>Full-time Lecturers</option>
             <option>Part-Time Lecturers</option>
             <option>Head of Laboratory</option>
        </select>
        </div>
        </div>


        <div class="12u">
                <div class="select-wrapper">
        <select name="demo-category" id="demo-category">
                 <option default selected="selected">-- Faculty --</option>
                 <option>Business School</option>
                 <option>School of Information Science and Technology</option>
             <option>School of Design</option>
             <option>Faculty of Education - Teachers College</option>
             <option>Faculty of Science and Technology</option>
             <option>School of Law</option>
             <option>Faculty of Liberal Arts</option>
             <option>Faculty of Medicine</option>
             <option>Conservatory of Music</option>
             <option>Faculty of Nursing and Allied Health Sciences</option>
             <option>Faculty of Psychology</option>
             <option>Faculty of Social and Political Science</option>
             <option>School of Hospitality and Tourism</option>
        </select>
                </div>
                </div>
                                            
                                            
                                            <div class="12u">
                                                                                                <div class="select-wrapper">
        <select name="demo-category" id="demo-category">
                 <option default selected="selected">-- Program of Study --</option>
                 <option>Business School</option>
                 <option>School of Information Science and Technology</option>
             <option>School of Design</option>
             <option>Faculty of Education - Teachers College</option>
             <option>Faculty of Science and Technology</option>
             <option>School of Law</option>
             <option>Faculty of Liberal Arts</option>
             <option>Faculty of Medicine</option>
             <option>Conservatory of Music</option>
             <option>Faculty of Nursing and Allied Health Sciences</option>
             <option>Faculty of Psychology</option>
             <option>Faculty of Social and Political Science</option>
             <option>School of Hospitality and Tourism</option>
             <option>Business School</option>
                 <option>School of Information Science and Technology</option>
             <option>School of Design</option>
             <option>Faculty of Education - Teachers College</option>
             <option>Faculty of Science and Technology</option>
             <option>School of Law</option>
             <option>Faculty of Liberal Arts</option>
             <option>Faculty of Medicine</option>
             <option>Conservatory of Music</option>
             <option>Faculty of Nursing and Allied Health Sciences</option>
             <option>Faculty of Psychology</option>
             <option>Faculty of Social and Political Science</option>
             <option>School of Hospitality and Tourism</option>
        </select>
                                                                                                </div>
                                                                                        </div>
                                            
                                            
                                        Main -->

