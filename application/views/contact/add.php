<!-- Main -->
<article id="main">
    <header>
        <h2>Add Contact Us</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="post" action="<?php echo base_url(); ?>contact/create">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="" required="required"/>
                        </div>
                        <div class="form-group">
                            <label>Position</label>
                            <textarea type="text" class="form-control" name="position" rows="5" required="required"></textarea>
                        </div>
                        <div class="form-group">
                            <br/>
                            <button>Submit</button>
                            <input type="button" value="Back" href="#" onclick="history.back();">
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</div>
</article>

