<!-- Main -->
<article id="main">
    <header>
        <h2>Edit Contact #<?php echo $contact->contact_id ?></h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="post" action="<?php echo base_url(); ?>contact/update">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="<?php echo $contact->contact_name?>"/>
                        </div>
                        <div class="form-group">
                            <label>Position</label>
                            <textarea type="text" class="form-control" name="position" rows="5"><?php echo $contact->contact_position?></textarea>
                        </div>
                        <div class="form-group">
                            <button>Submit</button>
                            <input type="hidden" class="form-control" name="id" value="<?php echo $contact->contact_id?>"/>
                        <input type="button" value="Back" href="#" onclick="history.back();">
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</div>
</article>

