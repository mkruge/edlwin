<!-- Main -->
<article id="main">
    <header>
        <h2>Contact Management</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <table style="border-style: hidden; border-color: none;">
                        <td><h3>Contact</h3></td>
                        <td style="text-align: right">
                            <a href="<?php echo base_url(); ?>contact/add" class="btn btn-default btn-md">
                                <i class="fa fa-plus-square"></i>
                                Add new contact
                            </a>
                        </td>
                    </table>
                    
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>id</td>
                            <td>Name</td>
                            <td>Position</td>
                            <td colspan="2">Action</td>
                        </tr>
                        <?php foreach ($contact as $n) { ?>
                              <tr>
                                  <td><?php echo $n->contact_id; ?></td>
                                  <td><?php echo $n->contact_name; ?></td>
                                  <td><?php echo $n->contact_position; ?></td>
                                  <td><a href="<?php echo base_url(); ?>contact/edit/<?php echo $n->contact_id; ?>" class="btn btn-primary btn-md">edit</a></td>
                                  <td><a href="<?php echo base_url(); ?>contact/delete/<?php echo $n->contact_id; ?>" class="btn btn-danger btn-md">delete</a></td>
                              </tr>
                          <?php } ?>
                    </table>
                    <p>
                        <br/>
                        <input type="button" value="Back" href="#" onclick="location.href='<?php echo base_url(); ?>general/my_admin'">
                    </p>
                </section>
            </div>
        </div>
    </section>
</div>
</article>

