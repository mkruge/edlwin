<!-- Main -->
<article id="main">
    <header>
        <h2>All Score</h2>
    </header>
    <section class="wrapper style5">
        <div class="templatemo-content-widget white-bg col-1 text-center templatemo-position-relative" style="width:25%; margin-left: auto;
             margin-right: auto;" >
            <center><img src="<?php echo base_url('assets/images/person.png') ?>" style="width:70%; vertical-align:middle" alt="Avatar" class="img-circle img-thumbnail margin-bottom-5"></center><br>
            <h2 class="text-uppercase blue-text margin-bottom-5" style="text-align:center; color:#102269"><?php echo $identity->EmployeeName; ?></h2>
            <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><?php echo $identity->EmployeeNo; ?></h5>
            <h5 class="text-uppercase margin-bottom-5" style="text-align:center"><b><?php echo $identity->Positions ?></b></h5>
            <h5 class="text-uppercase margin-bottom-30" style="text-align:center"><?php echo $identity->academicName_prodi ?></h5>
        </div>
        <br><br>
        <div class="12u">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Semester ID</td>
                        <td>Semester</td>
                        <td>Class</td>
                        <td>Subject</td>
                        <td>Mean</td>
                        <td>View</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($scores as $s) {
                        ?>
                          <tr>
                              <td><?php echo $s->term_ID ?></td>
                              <td><?php echo $s->term_title ?></td>
                              <td><?php echo $s->class_id ?></td>
                              <td><?php echo $s->subject_name ?></td>
                              <td><?php echo number_format($s->avg_score, 2) ?></td>
                              <td><a href="<?php echo base_url(); ?>scores/view/<?php echo $s->code ?>">Detail</a></td>
                          </tr>
                      <?php 
                    } ?>
                </tbody>
            </table>
        </div>
        <br>
        <div id="container"></div>
    </section>
</article>
<script type="text/javascript">
    Highcharts.chart('container', {
        
        title: {
            text: 'Score each semester'
        },
        subtitle: {
            text: ''
        },
        yAxis: {
            title: {
                text: 'Score Mean'
            }
        },
        xAxis: {
            categories: [<?php echo $cdata['categories'] ?>],
            crosshair: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        series: <?php echo json_encode($cdata['series']);?>

    });
</script>