<!-- Banner -->
<section id="banner">
    <div class="inner">
        <h2>Welcome</h2>
        <p>Here our news!</p>
    </div>
    <a href="#one" class="more scrolly">News More</a>
</section>

<!-- One -->
<section id="one" class="wrapper style1 special">
    <div class="inner">
        <header class="major">
            <h2>
                <?php echo nl2br($headline->news_title) ?>
            </h2>
            <p>
                <?php echo nl2br($headline->isi_news) ?>
            </p>
        </header>
        <ul class="icons major">
            <li><span class="icon fa-diamond major style1"><span class="label">Lorem</span></span></li>
            <li><span class="icon fa-heart-o major style2"><span class="label">Ipsum</span></span></li>
            <li><span class="icon fa-code major style3"><span class="label">Dolor</span></span></li>
        </ul>
    </div>
</section>

<!-- Two -->
<section id="two" class="wrapper alt style2">
    <?php
      $i = 1;
      foreach ($news as $n) {
          ?>
          <section class="spotlight">
              <div class="image">
                  <img src="<?php
                  if ($i % 2 == 0) {
                      echo base_url('assets/images/uph.jpg');
                  } else {
                     echo base_url('assets/images/pic01.jpg');
                  }
                  ?>" alt="" /></div><div class="content">
                  <h2><?php echo $n->news_title ?></h2>
                  <p>
                      <?php echo nl2br($n->news_title) ?>
                  </p>
              </div>
          </section>
          <?php
          $i++;
      }
    ?>
</section>