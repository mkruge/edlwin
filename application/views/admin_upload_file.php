<?php
foreach ($employee_identity as $identity)
  
  ?>

<!-- Main -->
<article id="main">
    <header>
        <h2>Upload File</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <h2 style="font-size: 25pt">Upload File</h2>
                    <form method="post" action="" enctype="multipart/form-data">
                        <div class="row uniform">
                              <div class="6u 12u">
                                <h4>Full Employee Name</h4>
                            </div>
                            <div class="6u 12u">
                                <p><?php echo $identity->EmployeeName ?></p>
                            </div>    
                            <div class="6u 12u">
                                <h4>Full Employee No</h4>
                            </div>
                            <div class="6u 12u">
                                <p><?php echo $identity->EmployeeNo ?></p>
                            </div>  
                            <div class="6u 12u">
                                <h4>Term</h4>
                            </div>
                            <div class="6u 12u">
                                <p><?php echo $sem->term_title ?> - <?php echo $sem->year ?></p>
                            </div>   

                            <div class="6u 12u">
                                <h4>Kelas</h4>
                            </div>
                            <div class="6u 12u">
                                <p>
                                    <select name="class_id">
                                        <option value=""></option>                                            
                                        <?php foreach ($kelas as $k) { ?>
                                          <option value="<?php echo $k->code ?>">
                                              <?php echo $k->code . '-' . $k->name ?>
                                          </option>                   
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                            <div class="6u 12u">
                                <h4>Subject</h4>
                            </div>
                            <div class="6u 12u">
                                <p>
                                    <select name="subject_id">
                                        <option value=""></option>                                            
                                        <?php foreach ($subjects as $subject) { ?>
                                          <option value="<?php echo $subject->subject_id ?>">
                                              <?php echo $subject->subject_code . '-' . $subject->subject_name ?>
                                          </option>                   
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                            <div class="6u 12u">
                                <h4>File</h4>
                            </div>
                            <div class="6u 12u">
                                <p><input type="file" name="userfile"/>
                                    <label id="filename"></label>
                                </p>
                            </div>
                            <div class="12u$" style="text-align:center">
                                <br/>
                                <br/>
                                <input type="hidden" name="semester_id" value="<?php echo $_GET['sem'] ?>"/>
                                <input type="hidden" name="lecturer_id" value="<?php echo $identity->EmployeeNo ?>"/>
                                <button type="submit" name="upload_score" class="special">Upload</button>
                            </div>
                            <p>
                                <br/>
                            </p>
                            <div class="12u$">
                                <ul class="actions">
                                    <li class="text-align:right">
                                        <button type="button" onclick="history.back(-1);">Back</button>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
    </section>
</div>
</article>


<script type="text/javascript">
  $('input[name="userfile"]').change(function () {
      var v = $(this).val();
      $("#filename").html(v);
  });

</script>