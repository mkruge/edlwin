<!-- Main -->
<article id="main">
    <header>
        <h2>Class Management</h2>
        <a href="<?php echo base_url('kelas/add') ?>">Add Class</a>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <?php if (count($kelas) == 0) { ?>
                          <div class="alert alert-danger">
                              No data found!
                          </div>
                      <?php } else { ?>
                          <table class="dataTable">
                              <thead>
                                  <tr>
                                      <th>Kode</th>
                                      <th>Nama</th>
                                      <th></th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($kelas as $n) { ?>
                                      <tr>
                                          <td><?php echo $n->code; ?></td>
                                          <td><?php echo $n->name; ?></td>
                                          <td width="3%"><a href="<?php echo base_url(); ?>kelas/edit/<?php echo $n->id; ?>" class="btn btn-primary btn-md">edit</a></td>
                                          <td width="3%"><a href="<?php echo base_url(); ?>kelas/delete/<?php echo $n->id; ?>" class="btn btn-danger btn-md">delete</a></td>
                                      </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                      <?php } ?>
                    <br/>
                    <br/>
                    <input type="button" value="Back" href="#" onclick="location.href = '<?php echo base_url(); ?>general/my_admin'">
                </section>
            </div>
        </div>
    </section>
</article>

