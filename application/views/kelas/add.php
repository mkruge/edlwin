<!-- Main -->
<article id="main">
    <header>
        <h2>Edit Class</h2>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="templatemo-content-widget no-padding" id="templatemo">
                <section>
                    <form method="post" class="validate" action="<?php echo base_url('kelas/create') ?>">
                        <div class="row uniform">
                            <div class="12u 12u">
                                <b>Class Name</b>
                                <input type="text" name="name" id="EmployeeName" value=""/>
                            </div>
                            <div class="12u 12u">
                                <b>Class Code</b>
                                <input type="text" name="code" id="EmployeeNo" value=""/>
                            </div>
                            <div class="12u">
                                <ul class="actions">
                                    <li><button type="submit" value="Save">Save</button></li>
                                    <li><input type="button" value="Back" href="#" onclick="history.back();"></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </section>
</article>

