<!-- Main -->
<article id="main">
    <header>
        <h2>List Lecturers</h2>
    </header>
    <section class="wrapper style5" style="padding: 1em 0 1em 0;">
        <form method="post" action="<?php echo base_url('login_process/editscore_button') ?>">
            <br><br><br>
            <center>
                <div class="templatemo-content-widget no-padding" id="templatemo">
                    <div class="panel panel-default table-responsive">
                        <table class="dataTable">
                            <thead>
                                <tr>
                                    <th>Lecturers ID</th>
                                    <th>Lecturers Name</th>
                                    <th>Term</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                  foreach ($hasilTransaksi as $dosen_list) {
                                      ?>
                                      <tr>
                                          <td style="width: 10%"><?php echo $dosen_list->EmployeeNo ?></td>
                                          <td><?php echo $dosen_list->EmployeeName ?></td>
                                          <td><?php echo $dosen_list->term_title ?> <?php echo $dosen_list->year ?></td>
                                          <td style="width: 7%">
                                              <a type="button" onclick="location.href = '<?php echo base_url('processing_form/uploadFile?id_lecture=' . $dosen_list->EmployeeNo .'&sem='.$dosen_list->term_ID) ?>'">
                                                  Upload
                                              </a>
                                          </td>
                                      </tr>  
                                  <?php } ?>
                            </tbody>
                        </table>
                        <br>
                        <input type="submit" value="Back" href="#" onclick="history.back();">
                    </div>
                </div>        
            </center>    
            <br><br><br>
        </form>
    </section>


</div>
</section>
</article>
