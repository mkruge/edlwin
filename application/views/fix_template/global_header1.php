<!DOCTYPE HTML>
<html>
	<head>
		<title>Quality Management Office UPH</title>
		<link rel="icon" href="UPH.png">
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/templatemo-style.css') ?>" />
        <script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
	</head>
	<body class="landing">

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<img src="<?php echo base_url('assets/images/LogoUPH(BackgroundGelap).png')?>" style="float:middle; margin-left:8px; margin-top:8px; margin-bottom:8px" width="90" height="35" alt="Logo UPH" />
						<nav id="nav" style="text-align: right;">

							<ul>
								<li class="special">
									<a href="#menu" class="menuToggle"><span>Menu</span></a>
									<div id="menu">
										<ul>
											<li> <a href="<?php echo base_url('general/home_page')?>"> Home</a> </li>
                                            <li> <a href="<?php echo base_url('general/display_account')?>"> My Account</a> </li>
                                            <li> <a href="<?php echo base_url('general/all_score_page')?>"> Score </a> </li>
                                            <li> <a href="<?php echo base_url('general/contact_us')?>"> Contact Us</a> </li>
                                            <li> <a href="<?php echo base_url('general/logout')?>">Logout</a> </li>
										</ul>
									</div>
								</li>
							</ul>
						</nav>
					</header>


