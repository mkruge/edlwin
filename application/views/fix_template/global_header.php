<!DOCTYPE HTML>
<html>
    <head>
        <title>Quality Management Office UPH</title>
        <link rel="icon" href="UPH.png">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
        <link href="<?php echo base_url('assets/css/templatemo-style.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/jquery-ui.css') ?>" rel="stylesheet">        
        <link href="<?php echo base_url('assets/css/jquery-ui.theme.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">        
        <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui.js') ?>"></script>        
        <script src="<?php echo base_url('assets/js/datatables.min.js') ?>"></script>        
        <script src="<?php echo base_url('assets/js/datatable.js') ?>"></script>        
        <script src="<?php echo base_url('assets/js/jquery.validate.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/loha.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/highcharts.js') ?>"></script>        
        <script src="<?php echo base_url('assets/js/exporting.js') ?>"></script>  
        <link href="<?php echo base_url('assets/css/datatables.min.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>" />
        
        
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body class="landing">

        <!-- Page Wrapper -->
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header" class="alt">
                <img src="<?php echo base_url('assets/images/LogoUPH(BackgroundGelap).png') ?>" style="float:middle; margin-left:8px; margin-top:8px; margin-bottom:8px" width="90" height="35" alt="Logo UPH" />
                <nav id="nav" style="text-align: right;">

                    <ul>
                        <li class="special">
                            <a href="#menu" class="menuToggle"><span>Menu</span></a>
                            <div id="menu">
                                <ul>
                                    <li> <a href="<?php echo base_url('home') ?>"> Home</a> </li>
                                    <li> <a href="<?php echo base_url('general/display_account') ?>"> My Account</a> </li>
                                    <?php
                                      $this->load->database();

                                      $this->db->select('*');
                                      $this->db->from('database');
                                      $this->db->where('EmployeeNo = ' . $_SESSION['employeeno'] . '');

                                      $get_employee = $this->db->get();
                                      $employee_result['employee'] = $get_employee->result();
                                      foreach ($employee_result['employee'] as $emp_privillage)
                                          if ($emp_privillage->user_type == 5) {
                                              //admin
                                              ?>
                                              <li> <a href="<?php echo base_url('scores/') ?>">All Score </a> </li>
                                              <li> 
                                                  Edit
                                                  <ul>
                                                      <li><a href="<?php echo base_url('processing_form/admin_edit') ?>"> Score </a> </li>
                                                      <li> <a href="<?php echo base_url('news/index') ?>"> News </a> </li>
                                                      <li> <a href="<?php echo base_url('kelas/index') ?>"> Class </a> </li>
                                                      <li> <a href="<?php echo base_url('dosen/index') ?>"> Lecturers </a> </li>
                                                      <li> <a href="<?php echo base_url('contact/index') ?>"> Contact </a> </li>
                                                  </ul>
                                                  <?php
                                              } elseif ($emp_privillage->user_type == 1) {
                                                  //dekan
                                                  ?>
                                              <li> <a href="<?php echo base_url('general/all_score_page') ?>"> All Score </a> </li>
                                              <li> <a href="<?php echo base_url('general/my_score') ?>"> My Score </a> </li>                                                   
                                              <?php
                                          } elseif ($emp_privillage->user_type == 2) {
                                              //kaprodi
                                              ?>
                                              <li> <a href="<?php echo base_url('general/all_score_page') ?>"> All Score </a> </li>
                                              <li> <a href="<?php echo base_url('general/my_score') ?>"> My Score </a> </li> 
                                              <?php
                                          } elseif ($emp_privillage->user_type == 3) {
                                              //dosen
                                              ?>
                                              <li> <a href="<?php echo base_url('general/my_score') ?>"> My Score </a> </li> 
                                          <?php } else { ?>
                                              <li> <a href="<?php echo base_url('general/all_score_page') ?>"> Score </a> </li>
                                          <?php }
                                    ?>
                                    <li> <a href="<?php echo base_url('general/logout') ?>">Logout</a> </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
                <?php if ($this->session->flashdata('msg')) { ?>
                      <div class="alert alert-success text-center" style="margin: auto; font-size: 12px; text-align: center; color: #f00;"><?php echo $this->session->flashdata('msg'); ?></div>
                  <?php } ?>
            </header>



