<!DOCTYPE HTML>
<!--
	Spectral by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Quality Management Office UPH</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/templatemo-style.css') ?>" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="landing">

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<img src="<?php echo base_url('assets/images/LogoUPH(BackgroundGelap).png') ?>" style="float:middle; margin-left:8px; margin-top:8px; margin-bottom:8px" width="90" height="35" alt="Logo UPH" />
						
					</header>

				