<?php
    foreach ($contact as $identity) 
?>
<!-- Main -->
					<article id="main">
						<header>
							<h2>Contact Us</h2>
						</header>
						<section class="wrapper style5">
							<div class="inner">

								
								<section>
									<h4 style="margin-left:10px">People to contact</h4>
							<div class="templatemo-content-widget no-padding" id="templatemo">
            						<div class="panel panel-default table-responsive">
										<table>
                                        <thead>
                                        	<tr>
                                        		<td><b>Position</b></td>
                                        		<td><b>Name</b></td>
                                        	</tr>
                                        </thead>

											<tbody>
												<?php 
                								foreach($contact as $new) { ?>
												<tr>
                    								<td><?php echo $new->contact_position ?></td>
                    								<td><?php echo $new->contact_name ?></td>
                 							    </tr> 
                 							    <?php } ?>
											</tbody>
										</table>
									</div>
							</div>

						</section>

							</div>
							</section>
					</article>